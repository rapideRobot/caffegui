#include <pangolin/pangolin.h>

int guiWidth = 640*2;
int guiHeight = 640;
int panelWidth = 200;

float linearPixelSample(const float * image, const int imageWidth, const float px, const float py) {

    int ix = px;
    int iy = py;
    float tx = px - ix;
    float ty = py - iy;
    return ((1-tx)*(1-ty)*image[ix + iy*imageWidth] +
           (1-tx)*ty*image[ix + (iy+1)*imageWidth] +
           tx*(1-ty)*image[ix+1 + iy*imageWidth] +
            tx*ty*image[ix+1 + (iy+1)*imageWidth]);

}

void logPolarImageSample(float * sampledImage, const float * input, const int inputWidth,
                         const float centerX, const float centerY,
                         const float minR, const float maxR,
                         const int nThetaSamples, const int nRhoSamples ) {

    float maxLogR = log(maxR);
    float minLogR = log(minR);
    float logStepR = (maxLogR-minLogR)/(float)(nRhoSamples-1);

    for (int r=0; r<nRhoSamples; ++r) {
        float radius = exp(minLogR + r*logStepR);
        for (int t=0; t<nThetaSamples; ++t) {
            float theta = t*M_PI*2.0/(float)nThetaSamples;
            float px = centerX + radius*cos(theta);
            float py = centerY + radius*sin(theta);
            sampledImage[r + t*nRhoSamples] = linearPixelSample(input,inputWidth,px,py);
        }
    }

}

int main (int argc, char * * argv) {

    const int imWidth = 200;
    const int imHeight = 200;

    pangolin::CreateGlutWindowAndBind("Main",guiWidth+panelWidth,guiHeight,GLUT_MULTISAMPLE | GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    glewInit();

    pangolin::CreatePanel("panel").SetBounds(0.0, 1.0, 0.0, pangolin::Attach::Pix(panelWidth));

    pangolin::View & imgDisp = pangolin::Display("image").SetAspect(1);
    pangolin::View & sampleDisp = pangolin::Display("sample").SetAspect(1);

    pangolin::Display("display")
            .SetBounds(1.0,0.0,1.0,pangolin::Attach::Pix(panelWidth))
            .AddDisplay(imgDisp)
            .AddDisplay(sampleDisp)
            .SetLayout(pangolin::LayoutEqual);

    pangolin::Var<float> minR("panel.minR",1,0.1,1);
    pangolin::Var<float> maxR("panel.maxR",80,10,100);
    pangolin::Var<int> nThetaSamples("panel.nThetaSamples",100,10,200);
    pangolin::Var<int> nRhoSamples("panel.nRhoSamples",100,10,200);
    pangolin::Var<float> centerX("panel.centerX",imWidth/2,imWidth/4,3*imWidth/4);
    pangolin::Var<float> centerY("panel.centerY",imHeight/2,imHeight/4,3*imHeight/4);

    pangolin::GlTexture * sampleTex = 0; //new pangolin::GlTexture(nThetaSamples,nRhoSamples);

    glPointSize(4);


    float * image = new float[imWidth*imHeight];
    for (int y=0; y<imHeight; ++y) {
        for (int x=0; x<imWidth; ++x) {
            image[x+y*imWidth] = ((x % 20) < 10) ^ ((y % 20) < 10);
//            image[x + y*imWidth] = x/(float)imWidth;
        }
    }
//    for (int i=0; i<imHeight; ++i) {
//        image[i + i*imWidth] = 1;
//    }

    pangolin::GlTexture imageTex(imWidth,imHeight);
    imageTex.Upload(image,GL_LUMINANCE,GL_FLOAT);

    for(int frame_num = 0 ; !pangolin::ShouldQuit() ; frame_num++) {

        if (nThetaSamples.GuiChanged() || nRhoSamples.GuiChanged() ||
                minR.GuiChanged() || maxR.GuiChanged() ||
                centerX.GuiChanged() || centerY.GuiChanged() || frame_num == 0) {
            delete sampleTex;
            sampleTex = new pangolin::GlTexture(nRhoSamples,nThetaSamples);

            float * sample = new float[nThetaSamples*nRhoSamples];
            logPolarImageSample(sample,image,imWidth,centerX,centerY,minR,maxR,nThetaSamples,nRhoSamples);
            sampleTex->Upload(sample,GL_LUMINANCE,GL_FLOAT);
            delete [] sample;
        }

        if (pangolin::HasResized()) {
            pangolin::DisplayBase().ActivateScissorAndClear();
        }

        glClearColor(1,1,1,1);
        imgDisp.ActivateScissorAndClear();

        glColor3f(1,1,1);
        imageTex.RenderToViewportFlipY();
        imgDisp.ActivatePixelOrthographic();

        glPushMatrix();
        glScalef(imgDisp.GetBounds().w / (float)imWidth, imgDisp.GetBounds().h / (float)imHeight, 1);

        glColor3f(0,0,1);
        glBegin(GL_POINTS);

        float maxLogR = log(maxR);
        float minLogR = log(minR);
        float logStepR = (maxLogR-minLogR)/(float)(nRhoSamples-1);

        for (int t=0; t<nThetaSamples; ++t) {
            for (int r=0; r<nRhoSamples; ++r) {
                float theta = t*M_PI*2.0/(float)nThetaSamples;
                float radius = exp(minLogR + r*logStepR);
                glVertex2f(centerX + radius*cos(theta),centerY + radius*sin(theta));
            }
        }
        glEnd();

        glPopMatrix();

        sampleDisp.ActivateScissorAndClear();

        glColor3f(1,1,1);
        sampleTex->RenderToViewportFlipY();

        glClearColor(0,0,0,1);


        pangolin::FinishGlutFrame();
    }
}
