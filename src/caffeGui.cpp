//#define CPU_ONLY

#include <pangolin/pangolin.h>
#include <GL/freeglut.h>
#include <iostream>
#include <Eigen/Dense>
#include <caffe/caffe.hpp>
#include <caffe/common.hpp>
#include <caffe/util/rng.hpp>
#include <caffe/util/upgrade_proto.hpp>
#include <vector_types.h>
#include <endian.h>
#include "helper_math.h"
#include <thrust/host_vector.h>
#include <pangolin/video/drivers/pango_video.h>
#include <pangolin/gl/gltexturecache.h>

#include "weightTransfer.h"
#include "denseNetGeneration.h"
#include <pangolin/video/drivers/openni2.h>
#include "utils.h"
#include <OpenNI.h>
#include <PS1080.h>

const int guiWidth = 1280;
const int guiHeight = 960;
const int panelWidth = 180;

typedef float ColorType;

template <typename DataType>
void computeDenseEmbedding(caffe::Net<float> * denseNet, std::vector<float3> & mappedDensePoints,
                           std::vector<int3> & im2Dproj, float * DataNotUsed,
                           DFusionDataset<DataType> * dataset, const int frameNum, const int subSample,
                           const int patchStride = 1
                           ) {
    
    const Image<DataType> & image = dataset->pyramid_32f[frameNum][subSample];
    Image<float4> denseVert;
    denseVert.ptr =  &(dataset->denseVertRender[frameNum*dataset->widthImage*dataset->heightImage]);
    denseVert.w = dataset->widthImage;
    denseVert.h = dataset->heightImage;
    
    boost::shared_ptr<caffe::MemoryDataLayer<float> > denseDataLayer = boost::static_pointer_cast<caffe::MemoryDataLayer<float> >(denseNet->layer_by_name("data"));
    boost::shared_ptr<caffe::Blob<float> > denseOutputBlob = denseNet->blob_by_name("feat");
    
    float * imageData = getCaffeinatedImage(image);
    
    denseDataLayer->Reset(imageData, DataNotUsed, 1);
    denseNet->ForwardFrom(0);
    
    int pad_h = (image.h - patchStride*denseOutputBlob->height())/2;
    int pad_w = (image.w - patchStride*denseOutputBlob->width())/2;

    //(h_i + 2 * pad_h - kernel_h) / stride_h + 1
    
    for (int h=0; h<denseOutputBlob->height(); ++h) {
        
        int y = (pad_h + patchStride*h+ 0.5) * (1 << subSample);
        //int y = (h) * (1 << subSample);
        for (int w=0; w<denseOutputBlob->width(); ++w) {
            int x = (pad_w + patchStride*w + 0.5) * (1 << subSample);
            //int x = (w) * (1 << subSample);
            float4 v = denseVert.ptr[x + y*denseVert.w];
            mappedDensePoints.push_back(make_float3(denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,0,h,w)],
                                        denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,1,h,w)],
                    denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,2,h,w)]));
            if(v.w != 0)
            {
                im2Dproj.push_back(make_int3(x,y,1));
            }else{
                im2Dproj.push_back(make_int3(x,y,0));
            }
        }
    }
}



void computeDenseEmbeddingFromImage(caffe::Net<float> * denseNet, std::vector<float3> & mappedDensePoints,
                                    std::vector<int3> & im2Dproj,
                                    float * DataNotUsed,
                                    Image<uchar3> & image_live,
                                    const int subSample) {


    Image<float> grey_tmp;
    grey_tmp.Alloc(image_live.w,image_live.h);
    std::vector< Image<float> >  pyramid;
    pyramid.resize(7);
    rgb2grey(image_live,grey_tmp);
    pyramid[0] = grey_tmp;
    for(int p = 1 ; p < 7;p++){
        const int w = image_live.w>>p;
        const int h = image_live.h>>p;
        pyramid[p].Alloc(w,h);
    }
    ComputeScalarPyramid(pyramid);

    Image<float> & image = pyramid[subSample];

    //    Image<float4> denseVert;
    //    denseVert.ptr =  &(dataset->denseVertRender[frameNum*dataset->widthImage*dataset->heightImage]);
    //    denseVert.w = dataset->widthImage;
    //    denseVert.h = dataset->heightImage;

    boost::shared_ptr<caffe::MemoryDataLayer<float> > denseDataLayer = boost::static_pointer_cast<caffe::MemoryDataLayer<float> >(denseNet->layer_by_name("data"));
    boost::shared_ptr<caffe::Blob<float> > denseOutputBlob = denseNet->blob_by_name("feat");

    float * imageData = getCaffeinatedImage(image);

    denseDataLayer->Reset(imageData, DataNotUsed, 1);
    denseNet->ForwardFrom(0);

    int pad_h = (image.h - denseOutputBlob->height())/2;
    int pad_w = (image.w - denseOutputBlob->width())/2;

    //(h_i + 2 * pad_h - kernel_h) / stride_h + 1

    for (int h=0; h<denseOutputBlob->height(); ++h) {

        int y = (pad_h + h+ 0.5) * (1 << subSample);
        //int y = (h) * (1 << subSample);
        for (int w=0; w<denseOutputBlob->width(); ++w) {
            int x = (pad_w + w + 0.5) * (1 << subSample);
            //int x = (w) * (1 << subSample);
            //float4 v = denseVert.ptr[x + y*denseVert.w];
            mappedDensePoints.push_back(make_float3(denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,0,h,w)],
                                        denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,1,h,w)],
                    denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,2,h,w)]));
            //if(v.w != 0)
            //{
            im2Dproj.push_back(make_int3(x,y,1));
            //            }else{
            //                im2Dproj.push_back(make_int3(x,y,0));
            //            }
        }
    }

}


void computeDenseEmbeddingFromImage(caffe::Net<float> * denseNet, std::vector<float3> & mappedDensePoints,
                                    std::vector<int3> & im2Dproj,
                                    float * DataNotUsed,
                                    Image<float> & image_live,
                                    const int subSample) {



    std::vector< Image<float> >  pyramid;
    pyramid.resize(7);
    pyramid[0] = image_live;
    for(int p = 1 ; p < 7;p++){
        const int w = image_live.w>>p;
        const int h = image_live.h>>p;
        pyramid[p].Alloc(w,h);
    }
    ComputeScalarPyramid(pyramid);

    Image<float> & image = pyramid[subSample];

    //    Image<float4> denseVert;
    //    denseVert.ptr =  &(dataset->denseVertRender[frameNum*dataset->widthImage*dataset->heightImage]);
    //    denseVert.w = dataset->widthImage;
    //    denseVert.h = dataset->heightImage;

    boost::shared_ptr<caffe::MemoryDataLayer<float> > denseDataLayer = boost::static_pointer_cast<caffe::MemoryDataLayer<float> >(denseNet->layer_by_name("data"));
    boost::shared_ptr<caffe::Blob<float> > denseOutputBlob = denseNet->blob_by_name("feat");

    float * imageData = getCaffeinatedImage(image);

    denseDataLayer->Reset(imageData, DataNotUsed, 1);
    denseNet->ForwardFrom(0);

    int pad_h = (image.h - denseOutputBlob->height())/2;
    int pad_w = (image.w - denseOutputBlob->width())/2;

    //(h_i + 2 * pad_h - kernel_h) / stride_h + 1

    for (int h=0; h<denseOutputBlob->height(); ++h) {

        int y = (pad_h + h+ 0.5) * (1 << subSample);
        //int y = (h) * (1 << subSample);
        for (int w=0; w<denseOutputBlob->width(); ++w) {
            int x = (pad_w + w + 0.5) * (1 << subSample);
            //int x = (w) * (1 << subSample);
            //float4 v = denseVert.ptr[x + y*denseVert.w];
            mappedDensePoints.push_back(make_float3(denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,0,h,w)],
                                        denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,1,h,w)],
                    denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,2,h,w)]));
            //if(v.w != 0)
            //{
            im2Dproj.push_back(make_int3(x,y,1));
            //            }else{
            //                im2Dproj.push_back(make_int3(x,y,0));
            //            }
        }
    }

}

// TODO
static Image<uchar3> obfuscatedDisplayImage;

template <typename DataType>
void computeDenseEmbeddingObfuscated(caffe::Net<float> * denseNet, std::vector<float3> & mappedDensePoints,
                                     std::vector<int3> & im2Dproj, float * DataNotUsed,
                                     DFusionDataset<DataType> * dataset, const int frameNum, const int subSample,
                                     bool obfuscateForeground
                                     ) {
    
    const Image<DataType> & image = dataset->pyramid_32f[frameNum][subSample];
    Image<float4> denseVert;
    denseVert.ptr =  &(dataset->denseVertRender[frameNum*dataset->widthImage*dataset->heightImage]);
    denseVert.w = dataset->widthImage;
    denseVert.h = dataset->heightImage;
    
    boost::shared_ptr<caffe::MemoryDataLayer<float> > denseDataLayer = boost::static_pointer_cast<caffe::MemoryDataLayer<float> >(denseNet->layer_by_name("data"));
    boost::shared_ptr<caffe::Blob<float> > denseOutputBlob = denseNet->blob_by_name("feat");
    
    int pad_h = (image.h - denseOutputBlob->height())/2;
    int pad_w = (image.w - denseOutputBlob->width())/2;
    
    float * imageData = getCaffeinatedImage(image);
    
    const int channels = 1; //sizeof(DataType)/sizeof(float);
    float * obfuscatedImageData = new float[image.w*image.h*channels];
    for (int h=0; h<image.h; ++h) {
        int y = (pad_h + h+ 0.5) * (1 << subSample);
        for (int w=0; w<image.w; ++w) {
            int x = (pad_w + w + 0.5) * (1 << subSample);
            float4 v = denseVert.ptr[x + y*denseVert.w];
            if ((v.w != 0 && obfuscateForeground) || (v.w == 0 && !obfuscateForeground) ) {
                for (int c=0; c<channels; ++c) {
                    obfuscatedImageData[w + h*image.w + c*image.w*image.h] = rand() / (float) RAND_MAX;
                }
            } else {
                for (int c=0; c<channels; ++c) {
                    obfuscatedImageData[w + h*image.w + c*image.w*image.h] = imageData[w + h*image.w + c*image.w*image.h];
                }
            }
        }
    }
    
    denseDataLayer->Reset(obfuscatedImageData, DataNotUsed, 1);
    denseNet->ForwardFrom(0);
    
    //(h_i + 2 * pad_h - kernel_h) / stride_h + 1
    
    for (int h=0; h<denseOutputBlob->height(); ++h) {
        
        int y = (pad_h + h+ 0.5) * (1 << subSample);
        //int y = (h) * (1 << subSample);
        for (int w=0; w<denseOutputBlob->width(); ++w) {
            int x = (pad_w + w + 0.5) * (1 << subSample);
            //int x = (w) * (1 << subSample);
            float4 v = denseVert.ptr[x + y*denseVert.w];
            mappedDensePoints.push_back(make_float3(denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,0,h,w)],
                                        denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,1,h,w)],
                    denseOutputBlob->cpu_data()[denseOutputBlob->offset(0,2,h,w)]));
            if(v.w != 0)
            {
                im2Dproj.push_back(make_int3(x,y,1));
            }else{
                im2Dproj.push_back(make_int3(x,y,0));
            }
        }
    }
    
    obfuscatedDisplayImage.Alloc(image.w,image.h,image.pitch);
    Image<float> obfuscatedTmpImage;
    obfuscatedTmpImage.w = image.w;
    obfuscatedTmpImage.h = image.h;
    obfuscatedTmpImage.ptr = obfuscatedImageData;
    imageFloatToChar(obfuscatedTmpImage ,obfuscatedDisplayImage);
    
    delete [] obfuscatedImageData;
    
}


template <typename DataType>
void computeDenseFlow(Image<int2> & flowImage, caffe::Net<float> * denseNet, const Image<DataType> & imageA, const Image<DataType> & imageB,
                      float * DataNotUsed) {
    
    std::vector<int2> im2DprojA, im2DprojB;
    std::vector<float3> mappedDensePointsA, mappedDensePointsB;
    
    //computeDenseEmbedding(denseNet,mappedDensePointsA,im2DprojA,DataNotUsed,imageA);
    
    // computeDenseEmbedding(denseNet,mappedDensePointsB,im2DprojB,DataNotUsed,imageB);
    
    for (int i=0; i<mappedDensePointsA.size(); ++i) {
        
        const float3 & featA = mappedDensePointsA[i];
        int closestBInd = -1;
        float closestBDist = 1e10;
        for (int j=0; j<mappedDensePointsB.size(); ++j) {
            const float3 & featB = mappedDensePointsB[j];
            const float dist = length(featA - featB);
            if (dist < closestBDist) {
                closestBDist = dist;
                closestBInd = j;
            }
            
        }
        //        std::cout << im2DprojA[i].x << ", " << im2DprojA[i].y << "  ->  " << im2DprojB[closestBInd].x << ", " << im2DprojB[closestBInd].y << std::endl;
        flowImage.ptr[im2DprojA[i].x + im2DprojA[i].y*flowImage.w] = im2DprojB[closestBInd];
    }
    
}

template <typename DataType>
void warpImage(Image<DataType> & warpedImage, const Image<DataType> & image, const Image<int2> & warpField) {
    
    int offsetX = (image.w - warpedImage.w) / 2;
    int offsetY = (image.h - warpedImage.h) / 2;
    
    for (int y=0; y<warpedImage.h; ++y) {
        for (int x=0; x<warpedImage.w; ++x) {
            int2 warpedPoint = warpField.ptr[x + y*warpField.w];
            warpedImage.ptr[x + y*warpedImage.w] = image.ptr[offsetX + warpedPoint.x + (offsetY + warpedPoint.y)*image.w];
        }
    }
    
}

void loopSampleTrainingPatches(bool * done, bool * nextPatchesReady, bool * lastPatchesUsed) {
    
    while ( !(*done) ) {
        
        while ( !(*lastPatchesUsed) && !(*done) ) {
            
        }
        
        if (*done) { break; }
        
        // sample training patches
        
        (*nextPatchesReady) = true;
        
    }
    
}

void ensureDatasetAvailable(std::map<std::string,DFusionDataset<ColorType> > & datasets, std::string directory_sn) {
    if (datasets.find(directory_sn) == datasets.end()) {
        std::cout << "loading " << directory_sn << std::endl;
        datasets[directory_sn] = DFusionDataset<ColorType>();
        loadDenseDFusionImages( datasets[directory_sn], directory_sn, [](const int frame) { return frame == 0; });//return (frame % 3) != 0; } );
    }
}

void forgetDataset(std::map<std::string,DFusionDataset<ColorType> > & datasets, std::string directory_sn) {
    std::cout << "dumping " << directory_sn << std::endl;
    auto it = datasets.find(directory_sn);
    datasets.erase(it);
}

int main(int argc, char** argv) {

    const bool useLiveCamera = false;
    std::string video_uri =  "openni2:[size=640x480]//";
    pangolin::VideoInput * video_ptr;

    pangolin::OpenNiVideo2 * openni_cam;
    if(useLiveCamera){
        video_ptr = new pangolin::VideoInput (video_uri);
        openni_cam = video_ptr->Cast<pangolin::OpenNiVideo2 >();
    }




    
    std::map<std::string,DFusionDataset<ColorType> > datasets;
    std::string initial_dataset = "/tmp/dfusion1/";
    datasets[initial_dataset] = DFusionDataset<ColorType>();
    
    DFusionDataset<ColorType> * dataset = &datasets[initial_dataset];
    DFusionDataset<ColorType> * testDataset = &datasets[initial_dataset];
    loadDenseDFusionImages( *dataset, initial_dataset , [](const int frame) { return (frame % 3) != 0; } );
    
    //    std::vector<float3> & canonicalVerts                     = dataset.canonicalVerts;
    //    std::vector<uchar3> & rgb8uc3                            = dataset.rgb8uc3;
    //    std::vector<float4> & denseVertRender                    = dataset.denseVertRender;
    //    std::vector<float4> & projectedVertCorrespondence        = dataset.projectedVertCorrespondence;
    //    std::vector< std::vector< Image<float> > > & pyramid_32f = dataset.pyramid_32f;
    
    
    int widthImage = testDataset->widthImage;
    int heightImage = testDataset->heightImage;
    int initialSubsample = 3;
    //    int numFrames = dataset->numFrames;
    
    //exit(0);
    
    ///tmp/dymData/right_eye_0464.png
    
    
    //    filenames.push_back("left_eye");
    //    filenames.push_back("right_eye");
    
    
    
    //    //    loadDFusionImages("/tmp/dymData/", 0, 453,filenames,allImages,allLabels,w,h);
    //   // loadDFusionImages("/tmp/dymData/small/", starting, ending,filenames,allImages,allLabels,allVerts,w,h);
    //   // numSemanticClasses = filenames.size();
    
    //    std::cout << "Read in " << std::endl;
    //    std::cout << w << " " << h  << std::endl;
    //    std::cout << allImages.size() << " " << allLabels.size()  << std::endl;
    
    //    FLAGS_alsologtostderr = 1;
    
    //    //    if (argc < 2) { std::cerr << "usage: caffeGui solver.prototxt" << std::endl; return 1; }
    
    caffe::GlobalInit(&argc,&argv);
    
    caffe::SolverParameter solverParam;
    //    //caffe::ReadProtoFromTextFileOrDie("examples/siamese/mnist_siamese_solver.prototxt", &solverParam);
    caffe::ReadProtoFromTextFileOrDie("networks/solver.prototxt", &solverParam);
    //    //    caffe::ReadProtoFromTextFileOrDie(std::string(argv[1]), &solverParam);
    
    
    //    //std::vector<unsigned char> testLabels, trainLabels;
    
    
    //    const int NumTrainingDataImages = allLabels.size()*0.9;
    //    const int NumTestingDataImages =  allLabels.size() - NumTrainingDataImages;
    
    //    const int NumTrainDataPairs =(NumTrainingDataImages/2);
    //    const int NumTestDataPairs =(NumTestingDataImages/2);
    
    //    //a hack to get the split pointer.
    //    float* testData =  allImages.data() + w*h*NumTrainingDataImages;
    //    float* testLabels = allLabels.data() + NumTrainingDataImages;
    
    //    float* trainData = allImages.data();
    //    float* trainLabels = allLabels.data();
    
    
    
    caffe::Caffe::SetDevice(0);
    caffe::Caffe::set_mode(caffe::Caffe::GPU);
    
    boost::shared_ptr<caffe::Solver<float> > solver(caffe::GetSolver<float>(solverParam));
    
    //    //load the test
    boost::shared_ptr<caffe::Net<float> > trainNet = solver->net();//test_nets()[0];// solver->net(); // there is only one training net.
    //    //    boost::shared_ptr<caffe::Net<float> > trainNet = solver->net(); //possibly multiple test nets
    
    boost::shared_ptr<caffe::Blob<float> > featABlob = trainNet->blob_by_name("feat");
    boost::shared_ptr<caffe::Blob<float> > featBBlob = trainNet->blob_by_name("feat_p");
    boost::shared_ptr<caffe::Blob<float> > pairBlob = trainNet->blob_by_name("pair_data");
    boost::shared_ptr<caffe::Blob<float> > lossBlob = trainNet->blob_by_name("loss");
    
    if (pairBlob->channels()/2 != sizeof(ColorType)/sizeof(float)) {
        std::cerr << "ColorType does not match network!" << std::endl;
        return -1;
    }
    


    caffe::NetParameter dynamicallyGeneratedTestNetParam;
    const int patchStride = 1;
    {
        caffe::NetParameter trainNetParam;
        caffe::ReadNetParamsFromTextFileOrDie(solverParam.net(),&trainNetParam);
        
        generateDenseTestNet(dynamicallyGeneratedTestNetParam,trainNetParam,pairBlob->channels()/2,widthImage >> initialSubsample,heightImage >> initialSubsample,trainNet, patchStride);
    }
    
//        caffe::Net<float> denseTestNet("networks/dense_test_1conv.prototxt",caffe::TEST);
    caffe::Net<float> denseTestNet(dynamicallyGeneratedTestNetParam);
    boost::shared_ptr<caffe::Blob<float> > denseInputBlob = denseTestNet.blob_by_name("data");
    boost::shared_ptr<caffe::Blob<float> > denseOutputBlob = denseTestNet.blob_by_name("feat");
    //    denseTestNet.ShareTrainedLayersWith(trainNet.get());
    
    std::cout << "denseInputBlob size: " << denseInputBlob->num() << ", " << denseInputBlob->channels() << ", " << denseInputBlob->height() << ", " << denseInputBlob->width() << std::endl;
    std::cout << "denseOutputBlob size: " << denseOutputBlob->num() << ", " << denseOutputBlob->channels() << ", " << denseOutputBlob->height() << ", " << denseOutputBlob->width() << std::endl;
    boost::shared_ptr<caffe::MemoryDataLayer<float> > denseDataLayer = boost::static_pointer_cast<caffe::MemoryDataLayer<float> >(denseTestNet.layer_by_name("data"));
    
    
    //    boost::shared_ptr<caffe::Layer<float> > loss = trainNet->layer_by_name("loss");
    
    //this is the batch size for the network --
    //note test net will have batch size that is set
    //to ensure that the complete testing data size is covered.
    //for example a testing sample size of N, will require
    //some batchsize = N/testIterations to ensure the total
    //error is measured for all N in batches of batchsize.
    
    //each blob has the same batch size (number of test or training samples.
    
    
    
    
    
    const int batch_size_training_pairs = pairBlob->num();
    const int batch_size_testing_pairs = pairBlob->num();
    
    const int num_training_batches = 1;
    //    const int num_testing_batches = NumTestDataPairs/batch_size_testing_pairs; //will equal one until we change batch size (or data).
    
    const int NumTrainingPairs = num_training_batches*batch_size_training_pairs;//  batch_size_training*2;
    //    const int NumTestingPairs = num_testing_batches*batch_size_testing_pairs;//  batch_size_training*2;
    //    //std::cout << allLabels.size() << ", " << batch_size_training_pairs << std::endl;
    
    int widthPatch = pairBlob->width();
    int heightPatch = pairBlob->height();
    int channels= pairBlob->channels(); //left and right
    
    std::cout << "========================" << std::endl;
    std::cout << "SIAMESE TRAINING NETWORK" << std::endl;
    std::cout << "========================" << std::endl;
    for(std::string n: trainNet->blob_names()){
        printBlobInfo(trainNet.get(),n);
    }
    
    std::cout << "========================" << std::endl;
    std::cout << "DENSE TEST NETWORK" << std::endl;
    std::cout << "========================" << std::endl;
    for(std::string n: denseTestNet.blob_names()){
        printBlobInfo( &denseTestNet,n);
    }
    
    
    int nD_output = featABlob->channels(); //i.e. 3d output...descriptor size
    
    
    //    int width_input_img = pairBlob->width();
    //    int height_input_img = pairBlob->height();
    
    
    //    // create dataset
    //    const int imgWidth = width_input_img;
    //    const int imgHeight = height_input_img;
    
    
    float * imgData = new float[NumTrainingPairs*channels*widthPatch*heightPatch];
    std::cout << "Num Channels " << channels << std::endl;
    //float* labelData = new float[NumTrainingPairs];
    
    //float * labelPairNums = new float [NumTrainingPairs*channels];
    std::vector<float>labelPairNums (NumTrainingPairs*channels,0);
    std::vector<int3> im2Dproj(NumTrainingPairs*channels,make_int3(0,0,1));
    
    
    float * flowData = new float[NumTrainingPairs];//for contastive loss, 1 per pair of training sample
    //float3 * flowData = new float3[NumTrainingPairs*channels];
    float * DataNotUsed = (float *)flowData;
    
    
    //    //1 add general mesh samples from dfusion
    //    //.5 add distances from the mesh data to a file.
    //    //2 think about occlusion
    //    //3 generalise caffeGui to nclasses
    
    
    
    
    std::random_device rd;
    std::mt19937 gen(rd());
    
    //    // values near the mean are the most likely
    //    // standard deviation affects the dispersion of generated values from the mean
    std::normal_distribution<> pert_distribution(0,1);//0,0.005);
    
    //    //load the training data in pairs.
    //    for (int i=0; i<NumTrainingPairs; ++i) {
    //        int a = rand() % NumTrainingDataImages;
    //        int b = rand() % NumTrainingDataImages;
    //        memcpy(imgData + (i*2+0)*imgWidth*imgHeight, trainData + a*imgWidth*imgHeight, imgWidth*imgHeight*sizeof(float));
    //        memcpy(imgData + (i*2+1)*imgWidth*imgHeight, trainData + b*imgWidth*imgHeight, imgWidth*imgHeight*sizeof(float));
    //        //labelData[i] = trainLabels[a] == trainLabels[b] ? 1 : 0;
    
    //        const float delta = length( allVerts[trainLabels[a]] - allVerts[trainLabels[b]]); // ? 1 : 0;
    //        flowData[i] = delta;
    
    //        std::cout << "LabeDistance " << i << " " << flowData[i] << std::endl;
    
    //        labelPairNums[i*2] = trainLabels[a];
    //        labelPairNums[i*2+1] =trainLabels[b];
    //    }
    
    
    boost::shared_ptr<caffe::MemoryDataLayer<float> > dataLayer = boost::static_pointer_cast<caffe::MemoryDataLayer<float> >(trainNet->layer_by_name("pair_data"));
    boost::shared_ptr<caffe::MemoryDataLayer<float> > dataFlowLayer = boost::static_pointer_cast<caffe::MemoryDataLayer<float> >(trainNet->layer_by_name("flow_data"));
    
    bool hasConvolution = false;
    boost::shared_ptr<caffe::Blob<float> > conv1WeightBlob, conv1BiasBlob, ip1WeightBlob;
    int conv1ParamIndex;
    {
        std::map<std::string,int>::const_iterator it = trainNet->param_names_index().find("conv1_w");
        if (it != trainNet->param_names_index().end()) {
            conv1ParamIndex = it->second;
            conv1WeightBlob = trainNet->params()[conv1ParamIndex];
            hasConvolution = true;
        }
        it = trainNet->param_names_index().find("conv1_b");
        if (it != trainNet->param_names_index().end()) {
            conv1BiasBlob = trainNet->params()[it->second];
        }
        it = trainNet->param_names_index().find("ip1_w");
        if (it != trainNet->param_names_index().end()) {
            ip1WeightBlob = trainNet->params()[it->second];
        }
    }
    
    pangolin::CreateGlutWindowAndBind("Main",guiWidth+panelWidth,guiHeight,GLUT_MULTISAMPLE | GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    
    glewInit();
    
    pangolin::OpenGlRenderState camState;//(pangolin::ProjectionMatrix(640,480,420,420,320,240,0.1,500));
    pangolin::OpenGlMatrixSpec glK = pangolin::ProjectionMatrixRDF_TopLeft(640,480,500,500,320,240,0.01,1000);
    camState.SetProjectionMatrix(glK);
    
    pangolin::OpenGlMatrixSpec id = pangolin::IdentityMatrix(pangolin::GlModelViewStack);
    //id.m[0]=-1;
    //const TooN::SE3<> identity;
    camState.SetModelViewMatrix(id);
    
    
    
    std::vector<float3>  mappedPoints ;
    std::vector<float3>  mappedDensePoints;
    ClickHandler3D * handler3d = new  ClickHandler3D(camState,mappedPoints);
    
    int & selectedExample = handler3d->selectedExample;
    
    
    pangolin::View& view3D = pangolin::Display("camera").SetAspect(640.0f/480.0f).SetHandler(handler3d); //pangolin::Handler3D(camState,pangolin::AxisNone,0.01,0.5));
    
    pangolin::View& view2D = pangolin::Display("image").SetAspect(640.0f/480.0f);
    
    pangolin::View& view2DB = pangolin::Display("image2").SetAspect(sizeof(ColorType)/sizeof(float) == 4 ? 2 : 1);
    
    //exampleDisp.ToggleShow();
    //pangolin::View& imgDisp = pangolin::Display("img").SetAspect(640.0f/480.0f);
    
    pangolin::DataLog trainingErrorLog;
    {
        std::vector<std::string> logLabels;
        logLabels.push_back("trainingError");
        trainingErrorLog.SetLabels(logLabels);
    }
    pangolin::Plotter trainingErrorPlotter(&trainingErrorLog,0,1000,0,0.05);
    
    pangolin::CreatePanel("panel").SetBounds(0.0, 1.0, 0.0, pangolin::Attach::Pix(panelWidth));
    
    pangolin::Display("display")
            .SetBounds(1.0,0.0,1.0,pangolin::Attach::Pix(panelWidth))
            .AddDisplay(view3D)
            .AddDisplay(view2D)
            .AddDisplay(view2DB)
            .AddDisplay(trainingErrorPlotter)
            .SetLayout(pangolin::LayoutEqual);
    
    pangolin::Var<bool> step("panel.step",false,true);
    pangolin::Var<bool> resetData("panel.resetData",false,false);
    //    pangolin::Var<bool> visLastData("panel.visLastData",false,true);
    //    pangolin::Var<bool> visRandTest("panel.visRandTest",false,true);
    pangolin::Var<int> frameNum("panel.frameNum",0,0,testDataset->numFrames-1);
    pangolin::Var<bool> updateDenseTest("panel.showDenseTest",true,true);
    pangolin::Var<int> patchPairNum("panel.patchPairNum",0,0,batch_size_training_pairs-1);
    pangolin::Var<float> radius("panel.radius",2,0.1, 2);
    pangolin::Var<int> rollingWindow("panel.rollingWindow",40,1,100);
    pangolin::Var<int> correspondenceWindow("panel.correspondenceWindow",1280,1,100);
    pangolin::Var<float> marginThreshold("panel.marginThreshold",0.2,0.1, 2);


    pangolin::Var<bool> densePrediction("panel.densePrediction",false,false);
    pangolin::Var<bool> live("panel.live",false,true);

    pangolin::Var<bool> resetBoundingBox("panel.resetBoundingBox",true,true);
    
    pangolin::Var<std::string> savedWeightsFile("panel.weightFile","/tmp/weights.protobuf");
    pangolin::Var<bool> saveWeights("panel.saveWeights",false,false);
    pangolin::Var<bool> loadWeights("panel.loadWeights",false,false);

    pangolin::Var<bool> reinitializeFilter("panel.reinitializeFilter",false,false);
    pangolin::Var<int> filterToReinitialize("panel.filterToReinitialize",0,0,conv1WeightBlob->num()-1);
    
    
    //    pangolin::Var<bool> showHideExample("panel.show/hide example",false,false);
    
    //    pangolin::Var<int> stepIters("panel.stepIters",1,1,1000);
    //    pangolin::Var<float> base_lr("panel.base_lr",0.001,0.0001, 0.1);
    //    pangolin::Var<float> moment("panel.moment",0.9,0.0001, 1);
    //    pangolin::Var<float> margin("panel.margin",0.9,0.0001, 1);
    
    //    pangolin::Var<bool> ringConstraint("panel.ringConstraint",false,true);
    //    pangolin::Var<bool>* useConstraints[10];
    //    for (int i=0; i<10; ++i) {
    //        char buff[24];
    //        sprintf(buff,"panel.use constraint %d",i);
    //        useConstraints[i] = new pangolin::Var<bool>(std::string(buff),true,true);
    //    }
    
    //    //pangolin::Var<bool> reset("panel.reset",false,false);
    
    pangolin::Var<bool> showCanonicalModelSpheres("panel.showCanonicalModelSpheres",false,true);
    pangolin::Var<bool> showFilter("panel.showFilter",false,true);
    pangolin::Var<bool> showFilterGradient("panel.showFilterGradient",false,true);
    pangolin::Var<int> layerToVisualize("panel.layerToVisualize",0,0,1);
    pangolin::Var<bool> showTestImage("panel.showTestImage",true,true);
    pangolin::Var<bool> ignoreInvalidVerts("panel.ignoreInvalidVerts",true,true);
    
    pangolin::Var<bool> showDenseMappedPoints("panel.showDenseMappedPoints",false,true);
    pangolin::Var<int> px("panel.px",widthImage/2,0, widthImage-1);
    pangolin::Var<int> py("panel.py", heightImage/2,  0, heightImage-1);
    pangolin::Var<int> subSample("panel.subSample", initialSubsample,  1, 6);
    pangolin::Var<float> baseLearningRate("panel.baseLR",solver->params().base_lr(),solver->params().base_lr()/10,solver->params().base_lr()*10,true);
    pangolin::Var<float> momentum("panel.momentum",solver->params().momentum(),0,1);
    
    pangolin::Var<int> patchSize("panel.patchSize",28,  16, 32);
    pangolin::Var<float> perturb_scale("panel.perturb_scale*",50,0, 10);
    
    pangolin::Var<bool> logPolarSampling("panel.logPolarSampling",false,true);
    pangolin::Var<float> minR("panel.minR",1,0.1,1);
    pangolin::Var<float> maxR("panel.maxR",5,10,100);
    
    
    std::vector<pangolin::Var<bool> * > trainDFusionX;
    std::vector<pangolin::Var<bool> * > testDFusionX;
    std::vector<std::string> directory_snX;
    
    
    
    for(int i = 0 ; i < 5 ;i++)    {
        std::string n_train = "panel.trainDFusion" + intToString(i+1,0);
        std::string n_test = "panel.testDFusion" + intToString(i+1,0);
        directory_snX.push_back("/tmp/dfusion" + intToString(i+1,0) + "/");
        trainDFusionX.push_back( new  pangolin::Var<bool>(n_train,i==0,true) );
        testDFusionX.push_back( new  pangolin::Var<bool>(n_test,i==0,true) );
        std::cout << directory_snX[i] << std::endl;
    }
    
    pangolin::Var<int> testFrameA("panel.testFrameA",0,0,200);
    pangolin::Var<int> testFrameB("panel.testFrameB",1,0,200);
    pangolin::Var<bool> doDenseFlow("panel.doDenseFlow",false,false);
    
    //    //   pangolin::Var<float> minY("panel.minY",-5,-10, -1);
    //    //   pangolin::Var<float> maxY("panel.maxY", 5,  1, 10);
    
    //    //num_batches = 200 will result in pushing 200 points through this training network.
    //    //pangolin::Var<int> num_batches("panel.batches",10,1,testLabels.size()/(2*batch_size));
    pangolin::Var<int> training_batch("panel.training batch",0,0,num_training_batches -1 );
    //    pangolin::Var<int> testing_batch("panel.testing batch",0,0,num_testing_batches-1);
    //    pangolin::Var<int> set_batch_size("panel.set batch size",batch_size_training_pairs,32,256);
    
    
    
    
    glEnable(GL_DEPTH_TEST);
    glClearColor(1.0,1.0,1.0,1.0);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    
    std::vector<int> randomSamples;
    
    int numSampleVerts = dataset->canonicalVerts.size();
    std::set<int> randomSamples_;
    
    {
        const int numCanonVerts = dataset->canonicalVerts.size();
        do{
            //1 sample a vertex from a cononical model
            int vertexIdx = rand() % numCanonVerts;
            randomSamples_.insert(vertexIdx);
        }while(randomSamples_.size()<numSampleVerts);
    }
    randomSamples.insert(randomSamples.end(),randomSamples_.begin(),randomSamples_.end());
    
    float3 min_vert = make_float3( std::numeric_limits<float>::max(),std::numeric_limits<float>::max(),std::numeric_limits<float>::max());
    float3 max_vert = make_float3( std::numeric_limits<float>::min(),std::numeric_limits<float>::min(),std::numeric_limits<float>::min());
    
    float3 min_vert_last = make_float3( std::numeric_limits<float>::max(),std::numeric_limits<float>::max(),std::numeric_limits<float>::max());
    float3 max_vert_last = make_float3( std::numeric_limits<float>::min(),std::numeric_limits<float>::min(),std::numeric_limits<float>::min());
    
    pangolin::Var<bool> showCorrespondences("ui_.showCorrespondences",false);
    pangolin::RegisterKeyPressCallback(' ', pangolin::ToggleVarFunctor("ui_.showCorrespondences")  );


    int depth_w = 640;
    int depth_h = 480;
    int rgb_w   = 640;
    int rgb_h   = 480;
    int video_bytes = 640*480*sizeof(uchar3);
    if(useLiveCamera){
        depth_w  = video_ptr->Streams()[0].Width();
        depth_h  = video_ptr->Streams()[0].Height();
        rgb_w    = video_ptr->Streams()[1].Width();
        rgb_h    = video_ptr->Streams()[1].Height();
        video_bytes = video_ptr->SizeBytes();
    }

    Image<uchar3> rgb_live;
    rgb_live.Alloc(rgb_w,rgb_h);

    Image<float> depth_live;
    depth_live.Alloc(rgb_w,rgb_h);

    std::vector<pangolin::Image<unsigned char> > imgs;

    std::cout << "depth " << depth_w << " rgb " << rgb_w << std::endl;

    std::unique_ptr<unsigned char[]> video_buffer(new unsigned char[video_bytes]);

    for(int pangolinFrame = 0 ; !pangolin::ShouldQuit() ; pangolinFrame++) {



        if (pangolin::HasResized()) {
            pangolin::DisplayBase().ActivateScissorAndClear();
        }
        
        if (pangolin::Pushed(saveWeights)) {
            caffe::NetParameter netParam;
            trainNet->ToProto(&netParam, false);
            caffe::WriteProtoToBinaryFile(netParam,savedWeightsFile);
        }
        if (pangolin::Pushed(loadWeights)) {
            caffe::NetParameter netParam;
            caffe::ReadProtoFromBinaryFile(savedWeightsFile,&netParam);
            
            // TODO
            //weightsGrayToRGB(netParam);
            //weightsRGBToGray(netParam);

            //weightsPadConv(netParam,"conv1",2,2);
            //weightsShaveInnerProduct(netParam,"ip1",2,2,32,27,27);
            
            trainNet->CopyTrainedLayersFrom(netParam);
        }
        
        //loss->layer_param().contrastive_loss_param().margin( margin);
        //        solver->params().set_base_lr(base_lr);
        //        solver->params().set_momentum(moment);
        
        //        if (pangolin::Pushed(showHideExample)) {
        //            exampleDisp.ToggleShow();
        //            pangolin::DisplayBase().ActivateScissorAndClear();
        //        }
        
        
        if (pangolin::Pushed(reinitializeFilter)) {
            caffe::Filler<float> * weightFiller = caffe::GetFiller<float>(trainNet->layer_by_name("conv1")->layer_param().convolution_param().weight_filler());
            caffe::Blob<float> tmpWeightBlob(1,conv1WeightBlob->channels(),conv1WeightBlob->height(),conv1WeightBlob->width());
            weightFiller->Fill(&tmpWeightBlob);
            memcpy(conv1WeightBlob->mutable_cpu_data() + filterToReinitialize*tmpWeightBlob.count(),tmpWeightBlob.cpu_data(),tmpWeightBlob.count()*sizeof(float));
            caffe::Filler<float> * biasFiller = caffe::GetFiller<float>(trainNet->layer_by_name("conv1")->layer_param().convolution_param().bias_filler());
            caffe::Blob<float> tmpBiasBlob(1,1,1,1);
            biasFiller->Fill(&tmpBiasBlob);
            memcpy(conv1BiasBlob->mutable_cpu_data() + filterToReinitialize*tmpBiasBlob.count(),tmpBiasBlob.cpu_data(),tmpBiasBlob.count()*sizeof(float));
        }

        // train options
        std::vector<DFusionDataset<ColorType>*> trainingDatasets;
        for(int i = 0 ; i < trainDFusionX.size();i++){
            pangolin::Var<bool> & train = *(trainDFusionX[i]);
            pangolin::Var<bool> & test = *(testDFusionX[i]);
            std::string directory_sn = directory_snX[i];
            if (train.GuiChanged() && pangolinFrame > 0) {
                
                if (train) {
                    ensureDatasetAvailable(datasets,directory_sn);
                    dataset = &datasets[directory_sn];
                } else if (!test) {
                    forgetDataset(datasets,directory_sn);
                }
            }
            
            if (test.GuiChanged() && pangolinFrame > 0) {
                if (test) {
                    for(int j = 0 ; j < trainDFusionX.size();j++){
                        if(i!=j){
                            pangolin::Var<bool> & test_other = *(testDFusionX[j]);
                            test_other = false;
                        }
                    }
                    ensureDatasetAvailable(datasets,directory_sn);
                    testDataset = &datasets[directory_sn];
                    
                    frameNum.Meta().range[1] =  testDataset->numFrames-1;
                    frameNum.Reset();
                    
                } else {
                    test = true;
                }
            }
            if (train) { trainingDatasets.push_back( &datasets[directory_sn] ); }
        }
        
        
        
        if(Pushed(resetData)){
            training_batch = 0;
            
            
            
            ////////////////////////////////////////////////////////
            ///generate a set of NumTrainingPairs patch pairs
            ///
            ///
            const bool useSingleNetwork = false;
            if(useSingleNetwork){
                //for training mapping from the input image data to the known continuous 3D vertex label.
                
                //                int trainingPairIdx = 0;
                //                do{
                
                
                
                //                    const int numCanonVerts = canonicalVerts.size();
                
                //                    //1 sample a vertex from a cononical model
                //                    //int vertexIdxA = rand() % numCanonVerts;
                //                    int randA = rand() % numSampleVerts;
                //                    int vertexIdxA = randomSamples[randA];
                
                
                //                    float3 p3D_A = canonicalVerts[vertexIdxA];
                
                
                //                    //for delta = 0.
                //                    //2 sample a frame that the vertex is visible in
                //                    //3 find a corresponding visible vertex in another frame (or the same frame), with distance delta.
                //                    int frameAIdx = rand() % numFrames;
                
                
                //                    //get the 2D locations of this vertex in both A and B
                //                    float4 p2D_A = projectedVertCorrespondence.data()[frameAIdx*numCanonVerts + vertexIdxA ];
                
                
                //                    if(p2D_A.w == 0 ) continue;
                
                //                    int2 P2D_A_ =  make_int2(p2D_A.x+ 0.5,p2D_A.y + 0.5);
                
                //                    //copy the patch pairs into the Convnet memory input blob.
                //                    //patch A
                //                    bool gotpatch = copyPatchFromImage(imgData + (trainingPairIdx*channels+0)*widthPatch*heightPatch,(uchar3 *) (&rgb8uc3.data()[ frameAIdx*widthImage*heightImage] ),
                //                            widthImage,heightImage,widthPatch,heightPatch, subSample, P2D_A_.x,P2D_A_.y);
                
                //                    if(gotpatch) {
                
                //                        flowData[trainingPairIdx*channels] = p3D_A;
                
                //                        labelPairNums[trainingPairIdx*channels]   =randA;
                
                //                        trainingPairIdx++;
                
                //                        //std::cout << "randA " << randA << " " << vertexIdxA << std::endl;
                
                //                    }else{
                
                //                    }
                
                //                }while(trainingPairIdx<NumTrainingPairs);
            }else{
                
                
                
                
                int trainingPairIdx = 0;
                
                const bool sampleBackground = false;
                static pangolin::Var<float> backgroundSamplingProbability("panel.backgroundSamplingProb",0.1,0,1);

                static pangolin::Var<float> sampleDifferentFrameProbability("panel.sampleDifferentFrameProbability",0.5,0,1);
                bool sampleDifferentFrame = (rand() / (float)RAND_MAX) < sampleDifferentFrameProbability;
                do{
                    
                    DFusionDataset<ColorType> * trainingDataset = trainingDatasets[rand() % trainingDatasets.size()];
                    const int numCanonVerts = trainingDataset->canonicalVerts.size();
                    
                    //1 sample a vertex from a cononical model
                    //int vertexIdxA = rand() % numCanonVerts;
                    
                    // sampling
                    //                    int randA = rand() % numSampleVerts;
                    //                    int randB = rand() % numSampleVerts;
                    //                    int vertexIdxA = randomSamples[randA];
                    //                    int vertexIdxB = randomSamples[randB];
                    
                    // all verts
                    int vertexIdxA = rand() % numCanonVerts;
//                    int vertexIdxB =  rand() % numCanonVerts;
                    int vertexIdxB = vertexIdxA;
                    
                    float3 p3D_A = trainingDataset->canonicalVerts[vertexIdxA];
                    float3 p3D_B = trainingDataset->canonicalVerts[vertexIdxB];
                    
                    //for delta = 0.
                    //2 sample a frame that the vertex is visible in
                    //3 find a corresponding visible vertex in another frame (or the same frame), with distance delta.
                    int frameAIdx = rand() % trainingDataset->numFrames;
//                    int frameBIdx = frameAIdx +  (rand() % rollingWindow) - rollingWindow/2;// //std::min(std::max(0, ,trainingDataset->numFrames-1);//  trainingDataset->numFrames;
                    //int frameBIdx = rand() % trainingDataset->numFrames;//
                    int frameBIdx = frameAIdx;

                    float2 P2D_perturb;
                    if (sampleDifferentFrame) {
                        frameBIdx = frameAIdx +  (rand() % rollingWindow) - rollingWindow/2;
                        if(frameBIdx < 0 || frameBIdx >= trainingDataset->numFrames) continue;

                        P2D_perturb = make_float2(0);
                    } else {
                        P2D_perturb =  make_float2( (rand()%correspondenceWindow) -  correspondenceWindow/2 ,
                                                                               (rand()%correspondenceWindow) -  correspondenceWindow/2);
                    }

                    //std::cout << "FrameA# " << frameAIdx << " FrameB# " << frameBIdx << std::endl;
                    
                    
                    //get the 2D locations of this vertex in both A and B
                    float4 p2D_A = trainingDataset->projectedVertCorrespondence.data()[frameAIdx*numCanonVerts + vertexIdxA ];
                    float4 p2D_B = trainingDataset->projectedVertCorrespondence.data()[frameBIdx*numCanonVerts + vertexIdxB ];
                    
                    //visibility check
                    if(p2D_B.w == 0 || p2D_A.w==0 ) continue;
                    
                    //float2  //perturb_scale*make_float2(pert_distribution(gen),pert_distribution(gen));

                    //                std::cout <<  "P2D_perturb " << P2D_perturb << std::endl;

                    const bool doDepthMapTest = false;
                    int2 P2D_A_ ;
                    int2 P2D_B_ ;
                    float delta;
                    if(doDepthMapTest){
                        P2D_A_ = make_int2(rand()%widthImage,rand()%heightImage);
                        P2D_B_ = P2D_A_ + make_int2(P2D_perturb);//  make_int2(rand()%widthImage,rand()%heightImage);

                        if(P2D_B_.x<0 || P2D_B_.y<0 || P2D_B_.x>=widthImage || P2D_B_.y>=heightImage ) continue;

                        //and now lets pick up 3D point in the dense maps
                        float d3D_A_ = trainingDataset->liveDepth.data()[frameAIdx*widthImage*heightImage + P2D_A_.x + P2D_A_.y*widthImage];
                        float d3D_B_ = trainingDataset->liveDepth.data()[frameBIdx*widthImage*heightImage + P2D_B_.x + P2D_B_.y*widthImage];
                        //std::cout <<  d3D_A_ << " " << d3D_B_ << std::endl;

                        float2 invFl = make_float2(1.0f/540.0,1.0f/540.0);
                        float2 pp = make_float2(320,240);
                        float3 v3D_A_ = make_float3((make_float2(P2D_A_) - pp)*invFl,1)*d3D_A_;
                        float3 v3D_B_ = make_float3((make_float2(P2D_B_) - pp)*invFl,1)*d3D_B_;

                        //check that both pixels are visible and within the image
                        if(d3D_A_<= 0 || d3D_B_<=0 ) continue;
                        delta = radius*length(v3D_A_-v3D_B_);

                    }else{

                        P2D_A_ =  make_int2(p2D_A.x+ 0.5,p2D_A.y + 0.5);
                        P2D_B_ =  make_int2(make_float2(p2D_B.x,p2D_B.y) + P2D_perturb + make_float2(0.5,0.5)) ; //we can add perterbation here so that delta>0.
//                        P2D_B_ =  make_int2(p2D_B.x+ 0.5,p2D_B.y + 0.5);



                        if(P2D_B_.x<0 || P2D_B_.y<0 || P2D_B_.x>=widthImage || P2D_B_.y>=heightImage ) continue;

                        //and now lets pick up 3D point in the dense maps
                        if (sampleDifferentFrame) {
                            float4 v3D_A_ = trainingDataset->denseVertRender.data()[frameAIdx*widthImage*heightImage + P2D_A_.x + P2D_A_.y*widthImage];
                            float4 v3D_B_ = trainingDataset->denseVertRender.data()[frameBIdx*widthImage*heightImage + P2D_B_.x + P2D_B_.y*widthImage];
                            if( (v3D_A_.w == 0 || v3D_B_.w==0) ) continue;
                            delta = radius*length(make_float3(v3D_A_-v3D_B_));
                        } else {

                            //and now lets pick up 3D point in the dense maps
                            //float d3D_A_ = p2D_A.z;
                            //float d3D_B_ = p2D_B.z;
                            float d3D_A_ = trainingDataset->liveDepth.data()[frameAIdx*widthImage*heightImage + P2D_A_.x + P2D_A_.y*widthImage];
                            float d3D_B_ = trainingDataset->liveDepth.data()[frameBIdx*widthImage*heightImage + P2D_B_.x + P2D_B_.y*widthImage];
                            //std::cout <<  d3D_A_ << " " << d3D_B_ << std::endl;

                            float2 invFl = make_float2(1.0f/540.0,1.0f/540.0);
                            float2 pp = make_float2(320,240);
                            float3 v3D_A_depth = make_float3((make_float2(P2D_A_) - pp)*invFl,1)*d3D_A_;
                            float3 v3D_B_depth = make_float3((make_float2(P2D_B_) - pp)*invFl,1)*d3D_B_;

                            //check that both pixels are visible and within the image
                            if(d3D_A_<= 0 || d3D_B_<=0 ) continue;
                            delta = radius*length(v3D_A_depth-v3D_B_depth);
                        }

                        //check that both pixels are visible and within the image

//                        delta = radius*length(p3D_A-p3D_B);


                    }

                    if(marginThreshold>0 && delta > marginThreshold){
                        delta = -marginThreshold;
                    }

                    //float delta = radius*length(p3D_A-p3D_B);
                    //float delta = radius*powf(length(p3D_A-p3D_B),1);
                    
                    
                    //                std::cout << "vertexIdx " << vertexIdx << std::endl;
                    
                    //                std::cout << "frameAIdx " << frameAIdx << std::endl;
                    //                std::cout << "frameBIdx " << frameBIdx << std::endl;
                    
                    //                std::cout << "p2D_A " << p2D_A << std::endl;
                    //                std::cout << "p2D_B " << p2D_B << std::endl;
                    
                    //                std::cout <<  "Canonical p3D " << p3D << std::endl;
                    //std::cout <<  "delta" << delta << std::endl;
                    
                    
                    //check visibility of A..B..
                    
                    //copy the patch pairs into the Convnet memory input blob.
                    bool gotpatch;
                    
                    if (sampleBackground && ((rand() / (float)RAND_MAX) < backgroundSamplingProbability) ) {
                        
                        static pangolin::Var<float> backgroundDistance("panel.backgroundDistance",1,0.1,10);
                        
                        //patch A
                        gotpatch = copyPatchFromPyramid(imgData + (trainingPairIdx*channels+0)*widthPatch*heightPatch, trainingDataset->pyramid_32f[frameAIdx] ,
                                                        widthPatch,heightPatch, subSample, P2D_A_.x,P2D_A_.y);
                        
                        int xB, yB;
                        do {
                            xB = widthPatch/2 + ( rand() % (widthImage - widthPatch));
                            yB = heightPatch/2 + ( rand() % (heightImage - heightPatch));
                        } while ( trainingDataset->denseVertRender.data()[frameBIdx*widthImage*heightImage + xB + yB*widthImage].w == 1 );
                        
                        gotpatch = copyPatchFromPyramid(imgData + (trainingPairIdx*channels+channels/2)*widthPatch*heightPatch, trainingDataset->pyramid_32f[frameBIdx] ,
                                                        widthPatch,heightPatch, subSample, xB, yB);
                        
                        flowData[trainingPairIdx] = backgroundDistance;
                    } else {
                        if (logPolarSampling) {
                            //patch A
                            gotpatch = copyPatchFromPyramidLogPolar(imgData + (trainingPairIdx*channels+0)*widthPatch*heightPatch, trainingDataset->pyramid_32f[frameAIdx] ,
                                                                    minR, maxR, widthPatch,heightPatch, subSample, P2D_A_.x,P2D_A_.y);
                            //patch B
                            gotpatch &= copyPatchFromPyramidLogPolar(imgData + (trainingPairIdx*channels+channels/2)*widthPatch*heightPatch, trainingDataset->pyramid_32f[frameBIdx] ,
                                                                     minR, maxR, widthPatch,heightPatch, subSample, P2D_B_.x,P2D_B_.y);
                        }
                        //                    else
                        else {
                            //patch A
                            gotpatch = copyPatchFromPyramid(imgData + (trainingPairIdx*channels+0)*widthPatch*heightPatch, trainingDataset->pyramid_32f[frameAIdx] ,
                                                            widthPatch,heightPatch, subSample, P2D_A_.x,P2D_A_.y);
                            //patch B
                            gotpatch &= copyPatchFromPyramid(imgData + (trainingPairIdx*channels+channels/2)*widthPatch*heightPatch, trainingDataset->pyramid_32f[frameBIdx] ,
                                                             widthPatch,heightPatch, subSample, P2D_B_.x,P2D_B_.y);
                        }
                        
                        flowData[trainingPairIdx] = delta;
                        //patch A
                        /*bool gotpatch = copyPatchFromImage(imgData + (trainingPairIdx*channels+0)*widthPatch*heightPatch,(uchar3 *) (&rgb8uc3.data()[ frameAIdx*widthImage*heightImage] ),
                                widthImage,heightImage,widthPatch,heightPatch, subSample, P2D_A_.x,P2D_A_.y);
                        //patch B
                        gotpatch &= copyPatchFromImage(imgData + (trainingPairIdx*channels+1)*widthPatch*heightPatch,(uchar3 *) (&rgb8uc3.data()[ frameBIdx*widthImage*heightImage] ),
                                widthImage,heightImage,widthPatch,heightPatch, subSample, P2D_B_.x,P2D_B_.y);*/
                    }
                    if(!gotpatch) continue;
                    
                    
                    //flowData[trainingPairIdx*channels +1] = p3D_B;
                    //flowData[trainingPairIdx] = delta;// powf(radius*delta,1.0);
                    
                    //        labelPairNums[trainingPairIdx*channels]   = vertexIdxA;
                    //        labelPairNums[trainingPairIdx*channels+1] = vertexIdxB;
                    
                    trainingPairIdx++;
                    sampleDifferentFrame = (rand() / (float)RAND_MAX) < sampleDifferentFrameProbability;

                }while(trainingPairIdx<NumTrainingPairs);
                
                //sampleTrainingPatches(trainingDatasets,radius,logPolarSampling,minR,maxR,subSample,widthPatch,heightPatch,channels,imgData,flowData,NumTrainingPairs);
            }
            
        }
        
        if (baseLearningRate.GuiChanged()) {
            solver->params().set_base_lr(baseLearningRate);
        }
        if (momentum.GuiChanged()) {
            solver->params().set_momentum(momentum);
        }
        
        
        const size_t offset_img_data_training =  batch_size_training_pairs*channels * heightPatch * widthPatch;  //offset to current batch dataset in imgData
        const size_t offset_label_data_training =  batch_size_training_pairs*1*1*1;
        if (step || pangolinFrame <1) {
            
            ////////////////////////////////////////////////////////
            ///Push the batch training set through gradient descent
            ///
            //specific_batches
            
            
            //dataLayer->Reset(imgData + size_img_data_training*training_batch,labelData + size_label_data_training*training_batch, batch_size_training);
            dataLayer->Reset(       (float*)imgData + offset_img_data_training*training_batch,          DataNotUsed, batch_size_training_pairs);
            dataFlowLayer->Reset(   (float *)(flowData + offset_label_data_training*training_batch),    DataNotUsed, batch_size_training_pairs);
            
            //step the solver
            //note: the batch size is set in the train_test.prototex file
            //this will run solver_type: SGD etc.
            std::cout << "STEP " <<  solver->iter() << " " << pangolinFrame << std::endl;
            if(pangolinFrame >0) solver->Step(1);
            
            const float loss = lossBlob->cpu_data()[0];
            std::cout << "loss: " << loss << std::endl;
            trainingErrorLog.Log(loss);
            
            //specific_batch = ((solver->iter() -1);
            training_batch = (training_batch +1)%num_training_batches;
            
            if(training_batch==0){
                resetData=true;
            }
            
            
            ////////////////////////////////////////////////////////
            ///Push the batch training set forward through network
            //if(frame_num%visNum ==0){
            
            //this is to be changed when we finally have a nice single channel test network.
            //size_t size_img_data_testing =  batch_size_testing_pairs*channels * height * width;
            //size_t size_label_data_testing =  batch_size_testing_pairs*1*1*1;
            //size_t
            
            mappedPoints.clear();
            //if(visLastData){
            
            bool doComputeBB =resetBoundingBox;// pangolin::Pushed(resetBoundingBox);
            min_vert = make_float3( std::numeric_limits<float>::max(),std::numeric_limits<float>::max(),std::numeric_limits<float>::max());
            max_vert = make_float3(-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max());
            for (int i=0; i<batch_size_training_pairs; i++) {
                
                float3 v1 = make_float3(featABlob->cpu_data()[nD_output*i],featABlob->cpu_data()[nD_output*i+1],featABlob->cpu_data()[nD_output*i+2]);
                float3 v2  =make_float3(featBBlob->cpu_data()[nD_output*i],featBBlob->cpu_data()[nD_output*i+1],featBBlob->cpu_data()[nD_output*i+2]);
                if(doComputeBB){
                    max_vert = fmaxf(max_vert, fmaxf(v1,v2));
                    min_vert = fminf(min_vert, fminf(v1,v2));
                }
                
                //get the resulting feature blob for image 1
                mappedPoints.push_back(v1);
                //get the resulting feature blob for image 2
                mappedPoints.push_back(v2);
                
                //mappedPoints.push_back(make_float3(featABlob->cpu_data()[nD_output*i],featABlob->cpu_data()[nD_output*i+1],0));
                //mappedPoints.push_back(make_float3(featBBlob->cpu_data()[nD_output*i],featBBlob->cpu_data()[nD_output*i+1],0));
            }
            
            min_vert_last = min_vert;
            max_vert_last = max_vert;
            
            
            
            std::cout << "mappedPoints " << mappedPoints.size() << std::endl;
            //                        }else{
            //                            if(visRandTest){
            //                                testing_batch =  rand() % num_testing_batches;
            //                            }
            //                            //dataLayer->Reset(testData + size_img_data_training*testing_batch,labelDataTest+ size_label_data_training*testing_batch, batch_size_training);
            
            //                            //set the starting pointer for this testing batch from TEST data
            //                            dataLayer->Reset(       testData + size_img_data_testing*testing_batch,   DataNotUsed    , batch_size_testing_pairs);
            
            //                            dataFlowLayer->Reset(   DataNotUsed,   DataNotUsed,batch_size_testing_pairs); //note
            //                            trainNet->ForwardFromTo(0, trainNet->layers().size()-2);//cut out the loss.
            
            
            //                            for (int i=0; i<batch_size_testing_pairs; i++) {
            //                                //get the resulting feature blob for image 1
            //                                mappedPoints.push_back(make_float3(featABlob->cpu_data()[nD_output*i],featABlob->cpu_data()[nD_output*i+1],featABlob->cpu_data()[nD_output*i+2]));
            //                                //get the resulting feature blob for image 2
            //                                mappedPoints.push_back(make_float3(featBBlob->cpu_data()[nD_output*i],featBBlob->cpu_data()[nD_output*i+1],featBBlob->cpu_data()[nD_output*i+2]));
            
            //                                //mappedPoints.push_back(make_float3(featABlob->cpu_data()[nD_output*i],featABlob->cpu_data()[nD_output*i+1],0));
            //                                //mappedPoints.push_back(make_float3(featBBlob->cpu_data()[nD_output*i],featBBlob->cpu_data()[nD_output*i+1],0));
            //                            }
            //                        }
            
            //                    }
            
            //if (visRandTest || visLastData) { selectedExample = -1; }
            
        }
        
        
        
        const bool frameNumGuiChanged =  frameNum.GuiChanged();
        if( Pushed(densePrediction)  || ( (updateDenseTest || frameNumGuiChanged ) && pangolinFrame > 0)) { // || (step && (pangolinFrame % 15 == 0)) ){
            //let's go and sample this image
            
            std::cout << "dense prediction " << std::endl;
            
            //            if(frameNumGuiChanged){
            
            //            }
            
            if (frameNum < testDataset->numFrames) {
                
                mappedDensePoints.clear();
                im2Dproj.clear();
                
                // copy params
                for (std::pair<std::string,int> paramNameAndIndex : denseTestNet.param_names_index()) {
                    //std::cout << "__dense__ " << paramNameAndIndex.first << ": " << paramNameAndIndex.second << std::endl;
                    boost::shared_ptr<caffe::Blob<float> > paramBlob = denseTestNet.params()[paramNameAndIndex.second];
                    std::cout << "\t" << paramNameAndIndex.first << ": " << paramBlob->num() << ", " << paramBlob->channels() << ", " << paramBlob->height() << ", " << paramBlob->width() << " (" << paramBlob->count() << ")" << std::endl;
                    auto it = trainNet->param_names_index().find(paramNameAndIndex.first);
                    if (it == trainNet->param_names_index().end()) {
                        std::cerr << "no sparse parallel" << std::endl;
                        continue;
                    }
                    //std::cout << "__sparse__ " << it->first << ": " << it->second << std::endl;
                    boost::shared_ptr<caffe::Blob<float> > sparseParamBlob = trainNet->params()[it->second];
                    //std::cout << "\t" << sparseParamBlob->num() << ", " << sparseParamBlob->channels() << ", " << sparseParamBlob->height() << ", " << sparseParamBlob->width() << " (" << sparseParamBlob->count() << ")"  << std::endl;
                    
                    caffe::caffe_copy(paramBlob->count(),sparseParamBlob->gpu_data(),paramBlob->mutable_gpu_data());
                }
                
                
                //                computeDenseEmbeddingObfuscated(&denseTestNet,mappedDensePoints,im2Dproj,DataNotUsed, testDataset,frameNum, subSample,false);//
                if(live && useLiveCamera){

                    if(video_ptr->Grab(video_buffer.get(), imgs, true, false)  ){//&& video.Grab(video_buffer.get(), imgs, true, false) && video.Grab(video_buffer.get(), imgs, true, false)){
                    }
                    Image<unsigned short> depth_live_dist( imgs[0].w, imgs[0].h, imgs[0].pitch, (unsigned short*)imgs[0].ptr);
                    for(int i = 0; i < imgs[0].w* imgs[0].h;i++){
                        depth_live.ptr[i] = (float)depth_live_dist.ptr[i]/1000.0;
                    }

                    Image<uchar3> rgb_live_tmp(imgs[1].w, imgs[1].h, imgs[1].pitch, (uchar3 *)imgs[1].ptr);
                    memcpy(rgb_live.ptr,rgb_live_tmp.ptr,rgb_live.pitch*rgb_live.h);


                    //rgb
                    //computeDenseEmbeddingFromImage(&denseTestNet,mappedDensePoints,im2Dproj,DataNotUsed, rgb_live_tmp, (int)subSample);//

                    //depth
                    computeDenseEmbeddingFromImage(&denseTestNet,mappedDensePoints,im2Dproj,DataNotUsed, depth_live, (int)subSample);//
                }else{
                    computeDenseEmbedding(&denseTestNet,mappedDensePoints,im2Dproj,DataNotUsed, testDataset,frameNum, subSample,patchStride);//
                }

                
                if(!step && resetBoundingBox){
                    min_vert = make_float3( std::numeric_limits<float>::max(),std::numeric_limits<float>::max(),std::numeric_limits<float>::max());
                    max_vert = make_float3(-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max());
                    for (int i=0; i<mappedDensePoints.size(); i++) {
                        float3 v1 =mappedDensePoints[i];
                        max_vert = fmaxf(max_vert, v1);
                        min_vert = fminf(min_vert, v1);
                    }
                    min_vert_last = min_vert;
                    max_vert_last = max_vert;
                }
            }
            
        }//end if densePrediction
        
//        if (Pushed(doDenseFlow)) {
            
//            Image<int2> flowImage;
//            Image<ColorType> & imageA = testDataset->pyramid_32f[testFrameA][subSample];
//            Image<ColorType> & imageB = testDataset->pyramid_32f[testFrameB][subSample];
            
//            flowImage.Alloc(denseOutputBlob->width(),
//                            denseOutputBlob->height(),
//                            denseOutputBlob->width()*sizeof(int2));
//            computeDenseFlow(flowImage,&denseTestNet,imageA,imageB,
//                             DataNotUsed);
            
//            Image<uchar3> charImageA;
//            charImageA.Alloc(imageA.w,imageA.h,imageA.w*sizeof(uchar3));
//            imageFloatToChar(imageA,charImageA);
            
//            Image<uchar3> charImageB;
//            charImageB.Alloc(imageB.w,imageB.h,imageB.w*sizeof(uchar3));
//            imageFloatToChar(imageB,charImageB);
            
//            Image<uchar3> imageBwarpedIntoImageA;
//            imageBwarpedIntoImageA.Alloc(denseOutputBlob->width(),
//                                         denseOutputBlob->height(),
//                                         denseOutputBlob->width()*sizeof(uchar3));
//            warpImage(imageBwarpedIntoImageA,charImageB,flowImage);
            
//            pangolin::SaveImage((pangolin::Image<unsigned char> &)(charImageA), pangolin::VideoFormatFromString("RGB24"),"/tmp/imageA.png");
//            pangolin::SaveImage((pangolin::Image<unsigned char> &)(charImageB), pangolin::VideoFormatFromString("RGB24"),"/tmp/imageB.png");
//            pangolin::SaveImage((pangolin::Image<unsigned char> &)(imageBwarpedIntoImageA), pangolin::VideoFormatFromString("RGB24"),"/tmp/warp.png");
            
//            imageBwarpedIntoImageA.Dealloc();
//            flowImage.Dealloc();
//            charImageA.Dealloc();
//            charImageB.Dealloc();
            
//        }
        
        view3D.ActivateScissorAndClear(camState);
        
        
        const bool showCanonicalModel  = false;
        if(showCanonicalModel){
            glColor3f(0,0,0);
            glPointSize(1);
            glBegin(GL_POINTS);
            for (int i=0; i<testDataset->canonicalVerts.size(); i++) {
                glVertex3f(testDataset->canonicalVerts[i].x,testDataset->canonicalVerts[i].y,testDataset->canonicalVerts[i].z);
            }
            
            glEnd();
            
            
            glColor3f(1,0,0);
            glPointSize(1);
            glBegin(GL_POINTS);
            
            for (int i=0; i<widthImage*heightImage; i++) {
                float4 v = testDataset->denseVertRender.data()[i + frameNum*widthImage*heightImage];
                if(v.w>0){
                    uchar3 c = testDataset->rgb8uc3.data()[i + frameNum*widthImage*heightImage];
                    glColor3ub(c.x,c.y,c.z);
                    glVertex3f(v.x,v.y,v.z);
                }
            }
            
            glEnd();
            
            {
                glColor3f(1,0,0);
                float4 v = testDataset->denseVertRender.data()[(int)px + (int)py*widthImage + (int)frameNum*widthImage*heightImage];
                if(v.w>0){
                    glPushMatrix();
                    glTranslatef(v.x,v.y,v.z);
                    glutSolidSphere(0.01,10,10);
                    glPopMatrix();
                }
            }
        }
        
        if(showCanonicalModelSpheres){
            for(int i = 0 ; i <randomSamples.size(); i++){
                int k = randomSamples[i];
                float4 c =glColorHSV(i/(float)numSampleVerts,1,1);
                glColor4fv(&c.x);
                float3 v = testDataset->canonicalVerts[k];
                glPushMatrix();
                glTranslatef(v.x,v.y,v.z);
                //glColor3f(v.x,v.y,v.z);
                glutSolidSphere(0.01,10,10);
                glPopMatrix();
            }
        }
        
        
        
        
        
        
        
        
        
        //imgDisp.ActivateScissorAndClear();
        //imgDisp.ActivatePixelOrthographic();
        
        
        //glPushMatrix();
        //glScalef(imgDisp.GetBounds().w/(maxX-minX),imgDisp.GetBounds().h/(maxY-minY),1);
        //glTranslatef(-minX,-minY,0);
        
        //        glColor3f(0,0,0);
        
        float3 avgVert = (min_vert + max_vert)/2.0;
        float3 rangeVert =  (max_vert - min_vert);
        float3 rangeVert_last =  (max_vert_last - min_vert_last);
        
        glColor4f(1,0,0,0.5);
        glPushMatrix();
        glTranslatef(avgVert.x,avgVert.y,avgVert.z);
        glScalef(rangeVert.x,rangeVert.y,rangeVert.z);
        glutWireCube(1);
        glPopMatrix();
        
        
        glPointSize(3);
        glBegin(GL_POINTS);
        //        int train_batch = (training_batch-1 + num_training_batches) % num_training_batches ;
        if (showDenseMappedPoints) {
            for (int i=0; i<mappedDensePoints.size(); i++) {
                float3 c = (mappedDensePoints[i] - min_vert)/rangeVert;
                
                int3 p2d = im2Dproj[i];
                if(p2d.z==1 || !ignoreInvalidVerts){
                    glColor4f(c.x,c.y,c.z,1);
                    glVertex3f(mappedDensePoints[i].x,mappedDensePoints[i].y,mappedDensePoints[i].z);
                }else{
                    //glColor4f(c.x,c.y,c.z,0.2);
                    //glVertex3f(mappedDensePoints[i].x,mappedDensePoints[i].y,mappedDensePoints[i].z);
                }

            }
            
        } else {
            for (int i=0; i<mappedPoints.size(); i++) {
                //int k =labelPairNums[i];
                //float4 c = glColorHSV(k/(float)numSampleVerts,1,1);
                //glColor4fv(&c.x);
                
                float3 c = (mappedPoints[i] - min_vert)/rangeVert;
                
                glColor3fv(&c.x);
                //pangolin::glColorHSV(360.0f*c.x,c.y,c.z);
                glVertex3f(mappedPoints[i].x,mappedPoints[i].y,mappedPoints[i].z);
            }
        }
        glEnd();
        
        //        if (exampleDisp.IsShown() && selectedExample >= 0) {
        //            //            glPushMatrix();
        //            //            glColor3f(0.5,0.5,0.5);
        
        //            if(visLastData){
        //                float4 c = glColorHSV(labelPairNums[ ((train_batch)*batch_size_testing_pairs*2 + selectedExample)%(2*NumTrainingPairs)]/numSemanticClasses);
        //                glColor4f(c.x,c.y,c.z,0.5);
        //            }else{
        //                //using the unshufftled test data
        //                float4 c = glColorHSV(testLabels[ (testing_batch*batch_size_testing_pairs*2 + selectedExample)%(NumTestingDataImages)]/numSemanticClasses);
        //                glColor4f(c.x,c.y,c.z,0.5);
        //            }
        
        //            //            glTranslatef(mappedPoints[selectedExample].x,mappedPoints[selectedExample].y,mappedPoints[selectedExample].z);
        //            //            glutWireSphere(0.02,8,8);
        //            //            glPopMatrix();
        
        //            glPointSize(10);
        //            glBegin(GL_POINTS);
        //            //glTranslatef(mappedPoints[selectedExample].x,mappedPoints[selectedExample].y,mappedPoints[selectedExample].z);
        //            //glutWireSphere(0.02,8,8);
        //            //glPopMatrix();
        
        //            glVertex3f(mappedPoints[selectedExample].x,mappedPoints[selectedExample].y,mappedPoints[selectedExample].z);
        //            glEnd();
        
        //        }
        //bottom
        //glPopMatrix();
        
        
        
        //        //selectedExample = min(max(0,visNum),nPairs*2);
        //        glColor3f(1,1,1);
        //        if (exampleDisp.IsShown() && selectedExample >= 0) {
        
        //            if (visLastData) {
        //rgb_8u_c3_tex.Upload(&imgData[selectedExample*width_input_img*height_input_img]);
        //            } else {
        //                exampleTex.Upload(&testData[((testing_batch*batch_size_testing_pairs*2 + selectedExample)*width_input_img*height_input_img)],GL_LUMINANCE,GL_FLOAT);
        //            }
        
        
        
        //            exampleDisp.ActivateScissorAndClear();
        //            exampleTex.RenderToViewportFlipY();
        //        }
        
        
        //glDepthMask(false);
        glPushMatrix();
        glTranslated(0,0,0);
        //glutWireCube(1);
        glColor4f(0.6,0.6,0.6,0.5);
        pangolin::glDraw_y0(0.1,10);
        glColor4f(0.8,0.8,0.8,0.5);
        pangolin::glDraw_y0(0.2,50);
        glPopMatrix();
        //glEnable(GL_DEPTH);
        //glDepthMask(true);
        
        
        
        ////////////////////////////////////////////
        /// 2D Rendering
        glColor3f(1,1,1);
        view2D.ActivateScissorAndClear();
        
        if (showTestImage) {
            //            RenderToViewport((uchar3*) obfuscatedDisplayImage.ptr,obfuscatedDisplayImage.w,obfuscatedDisplayImage.h,false,true,true);
            
            if(live && useLiveCamera){
                //RenderToViewport( (uchar3 *) (rgb_live.ptr),rgb_live.w,rgb_live.h,false,true,true);
                RenderToViewport( (float *) (depth_live.ptr),depth_live.w,depth_live.h,false,true,true);

            }else{
                RenderToViewport( (uchar3 *) (&testDataset->rgb8uc3.data()[ frameNum*widthImage*heightImage] ),widthImage,heightImage,false,true,true);
            }
        } else {
            
            //show the patch pair
            showPatchPair(imgData,patchPairNum,channels, widthPatch, heightPatch);
            //            Image<float> & im = dataset->pyramid_32f[frameNum][subSample];
            //            RenderToViewport( im.ptr,im.w,im.h,false,true,true);
        }
        
        
        //view2D.ActivatePixelOrthographic();
        
        if (showTestImage) {
            startDrawing(widthImage,heightImage);
            
            glPointSize(5);
            glBegin(GL_POINTS);
            for (int i=0; i<mappedDensePoints.size(); i++) {
                //int k =labelPairNums[i];
                //float4 c = glColorHSV(k/(float)numSampleVerts,1,1);
                //glColor4fv(&c.x);
                float3 c = (mappedDensePoints[i] - min_vert_last)/rangeVert_last;
                
                //pangolin::glColorHSV(360.0f*c.x,c.y,c.z);
                
                
                int3 p2d = im2Dproj[i];
                if(p2d.z==1 || !ignoreInvalidVerts){
                    glColor4f(c.x,c.y,c.z,1);
                }else{
                    glColor4f(c.x,c.y,c.z,0.5);
                }
                glVertex2f(p2d.x,p2d.y);
                //std::cout << i << " " << mappedDensePoints[i] << std::endl;
            }
            glEnd();
            
            endDrawing();
        } else {
            startDrawing(widthPatch,heightPatch*2);
            
            glPointSize(10);
            glColor3f(1,0,0);
            
            glBegin(GL_POINTS);
            glVertex2f(widthPatch/2,heightPatch/2);
            glVertex2f(widthPatch/2,3*heightPatch/2);
            glEnd();
            
            endDrawing();
            
        }
        
        view2DB.ActivateScissorAndClear();
        if (hasConvolution && showFilter) {
            
            const static int filterWidth = conv1WeightBlob->width();
            const static int filterHeight = conv1WeightBlob->height();
            static float * filterViz = new float[filterWidth*filterHeight*channels/2];
            
            if (layerToVisualize == 0) {
                static pangolin::Var<int> filterNum("panel.filterNum",conv1WeightBlob->num(),0,conv1WeightBlob->num());
                if (filterNum == conv1WeightBlob->num()) {
                    if (showFilterGradient) {
                        showAllFilterGradientVisualization(channels,filterWidth,filterHeight,conv1WeightBlob->cpu_diff(),conv1WeightBlob->num());
                    } else {
                        showAllFilterVisualization(channels,filterWidth,filterHeight,conv1WeightBlob->cpu_data(),conv1WeightBlob->num());
                    }
                } else {
                    showFilterVisualization(channels,filterWidth,filterHeight,(int)filterNum,filterViz,conv1WeightBlob->cpu_data(),conv1WeightBlob->num());
                }
            } else {
                static pangolin::Var<int> inputChannelToVisualize("panel.channelToVisualize",0,0,conv1WeightBlob->num()-1);

                showAllInnerProductsVisualization(ip1WeightBlob->cpu_data(),ip1WeightBlob->num(),
                                                  trainNet->blob_by_name("conv1")->channels(),
                                                  trainNet->blob_by_name("conv1")->width(),
                                                  trainNet->blob_by_name("conv1")->height(),
                                                  inputChannelToVisualize);
            }

            //            std::cout << conv1ParamBlob->cpu_data()[0] << std::endl;
            
            //            std::map<std::string,int>::const_iterator it = trainNet->param_names_index().find("conv1_w");
            //            if (it != trainNet->param_names_index().end()) {
            //                int ip1ParamIndex = it->second;
            //                std::cout << "conv1 param index " << ip1ParamIndex << std::endl;
            //                boost::shared_ptr<caffe::Blob<float> > ip1ParamBlob = trainNet->params()[ip1ParamIndex];
            //                std::cout << ip1ParamBlob->num() << " " << ip1ParamBlob->channels() << " " <<
            //                             ip1ParamBlob->height() << " " << ip1ParamBlob->width() << std::endl;
            //                const int filterWidth = ip1ParamBlob->width();
            //                const int filterHeight = ip1ParamBlob->height();
            //                static pangolin::Var<int> filterNum("panel.filterNum",0,0,ip1ParamBlob->num()-1);
            //                float filterWeights[filterWidth*filterHeight];
            //                float min = 1e20;
            //                float max = -1e20;
            //                for (int y=0; y<filterHeight; ++y) {
            //                    for (int x=0; x<filterWidth; ++x) {
            //                        filterWeights[x + y*filterWidth] =
            //                                ip1ParamBlob->cpu_data()[filterNum*filterWidth*filterHeight + x + y*filterWidth];
            //                        min = std::min(min,filterWeights[x + y*filterWidth]);
            //                        max = std::max(max,filterWeights[x + y*filterWidth]);
            //                    }
            //                }
            //                for (int y=0; y<filterHeight; ++y) {
            //                    for (int x=0; x<filterWidth; ++x) {
            //                        filterWeights[x + y*filterWidth] = (filterWeights[x + y*filterWidth] - min)/(max-min);
            //                    }
            //                }
            //                glColor3f(1,1,1);
            //                RenderToViewport(filterWeights,filterWidth,filterHeight,false,true,false);
            //            }
            //            conv1ParamBlob = trainNet->params()[conv1ParamIndex];
            
            
        }
        
        
        pangolin::FinishGlutFrame();
        
        {
            if(showCorrespondences ){
                
                handler3d->enable3DMotion=false;
            }else{
                
                handler3d->enable3DMotion=true;
            }
            
            //lastP3D = handler3d->p3Dclick;
            //if(showCorrespondences )
            {
                
                if(showCorrespondences ){
                    //lastP3D = handler3d->p3Dclick;
                }
            }
            
            
        }
        
    }

    delete video_ptr;
    return 0;
}

