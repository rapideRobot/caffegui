#ifndef WEIGHTTRANSFER_H
#define WEIGHTTRANSFER_H

#include <string>
#include <caffe/caffe.hpp>

bool getLayerAndPairIndex(int & layerIndex, int & pairLayerIndex, const caffe::NetParameter & netParam, const std::string & layerName) {
    for (int i=0; i < netParam.layer_size(); ++i) {
        const caffe::LayerParameter & layerParam = netParam.layer(i);
        if (layerParam.name() == layerName) {
            layerIndex = i;
        } else if (layerParam.name() == layerName + "_p") {
            pairLayerIndex = i;
        }
    }
    if (layerIndex == -1) {
        std::cerr << "could not find layer " << layerName << "!" << std::endl;
        return false;
    }
    if (pairLayerIndex == -1) {
        std::cerr << "could not find layer " << layerName << "_p!" << std::endl;
        return false;
    }
    return true;
}

void printNetParameter(const caffe::NetParameter netParam) {
    for (int i=0; i < netParam.layer_size(); ++i) {
        const caffe::LayerParameter & layerParam = netParam.layer(i);
        std::cout << layerParam.name() << std::endl;
        for (int j=0; j<layerParam.blobs_size(); ++j) {
            const caffe::BlobProto & blob = layerParam.blobs(j);
            std::cout << blob.shape().dim(0);
            for (int k=1; k < blob.shape().dim_size(); ++k) {
                std::cout << " x " << blob.shape().dim(k);
            }
            std::cout << std::endl;
        }
    }
}

void weightsGrayToRGBConv(caffe::NetParameter & param, const std::string layerName) {

    int layerIndex = -1;
    int _pLayerIndex = -1;
    if (!getLayerAndPairIndex(layerIndex,_pLayerIndex,param,layerName)) { return; }

    caffe::BlobProto * weightBlob = param.mutable_layer(layerIndex)->mutable_blobs(0);
    if (weightBlob->shape().dim(1) != 1) {
        std::cerr << "layer " << layerName << " is not grayscale!" << std::endl;
        return;
    }

    caffe::BlobProto * _pWeightBlob = param.mutable_layer(_pLayerIndex)->mutable_blobs(0);

    weightBlob->mutable_shape()->set_dim(1,3);
    _pWeightBlob->mutable_shape()->set_dim(1,3);

    const int originalSize = weightBlob->data_size();
    for (int i=0; i<2*originalSize; ++i) {
        weightBlob->add_data(0);
        weightBlob->add_diff(0);
        _pWeightBlob->add_data(0);
        _pWeightBlob->add_diff(0);
    }

    const int num = weightBlob->shape().dim(0);
    const int channels = 3;
    const int height = weightBlob->shape().dim(2);
    const int width = weightBlob->shape().dim(3);

    for (int n=num-1; n>=0; --n) { // fill in from the back so we can go in place
        for (int h=0; h<height; ++h) {
            for (int w=0; w<width; ++w) {
                for (int c=0; c<channels; ++c) {
                    weightBlob->set_data(w + h*width + c*width*height + n*width*height*channels,weightBlob->data(w + h*width + n*width*height));
                    _pWeightBlob->set_data(w + h*width + c*width*height + n*width*height*channels,_pWeightBlob->data(w + h*width + n*width*height));
                }
            }
        }

    }

    printNetParameter(param);

}

void weightsRGBToGrayConv(caffe::NetParameter & param, const std::string layerName) {

    int layerIndex = -1;
    int _pLayerIndex = -1;
    if (!getLayerAndPairIndex(layerIndex,_pLayerIndex,param,layerName)) { return; }

    caffe::BlobProto * weightBlob = param.mutable_layer(layerIndex)->mutable_blobs(0);
    if (weightBlob->shape().dim(1) != 3) {
        std::cerr << "layer " << layerName << " is not RGB!" << std::endl;
        return;
    }

    caffe::BlobProto * _pWeightBlob = param.mutable_layer(_pLayerIndex)->mutable_blobs(0);

    weightBlob->mutable_shape()->set_dim(1,1);
    _pWeightBlob->mutable_shape()->set_dim(1,1);

    const int num = weightBlob->shape().dim(0);
    const int height = weightBlob->shape().dim(2);
    const int width = weightBlob->shape().dim(3);

    float * rgbWeights = new float[num*3*height*width];
    for (int i=0; i<num*3*height*width; ++i) {
        rgbWeights[i] = weightBlob->data(i);
    }
    weightBlob->clear_data();
    weightBlob->clear_diff();
    _pWeightBlob->clear_data();
    _pWeightBlob->clear_diff();

    float oneThird = 1.f/3;
    for (int n=0; n<num; ++n) {
        for (int h=0; h<height; ++h) {
            for (int w=0; w<width; ++w) {
                float gray = 0;
                for (int c=0; c<3; ++c) {
                    gray += rgbWeights[w + h*width + c*width*height + n*width*height*3];
                }
                gray *= oneThird;
                weightBlob->add_data( gray );
                weightBlob->add_diff(0);
                _pWeightBlob->add_data( gray );
                _pWeightBlob->add_diff(0);
            }
        }

    }

    printNetParameter(param);

}

void weightsResizeConv(caffe::NetParameter & param, const std::string layerName,
                       const int newWidth, const int newHeight) {

    int layerIndex = -1;
    int _pLayerIndex = -1;
    if (!getLayerAndPairIndex(layerIndex,_pLayerIndex,param,layerName)) { return; }

    param.mutable_layer(layerIndex)->mutable_convolution_param()->set_kernel_size(newWidth);
    param.mutable_layer(_pLayerIndex)->mutable_convolution_param()->set_kernel_size(newWidth);

    caffe::BlobProto * weightBlob = param.mutable_layer(layerIndex)->mutable_blobs(0);
    caffe::BlobProto * _pWeightBlob = param.mutable_layer(_pLayerIndex)->mutable_blobs(0);

    const int originalSize = weightBlob->data_size();
    float * originalData = new float[originalSize];
    for (int i=0; i<originalSize; ++i) { originalData[i] = weightBlob->data(i); }

    const int num = weightBlob->shape().dim(0);
    const int channels = weightBlob->shape().dim(1);
    const int height = weightBlob->shape().dim(2);
    const int width = weightBlob->shape().dim(3);

    weightBlob->clear_data();
    weightBlob->clear_diff();
    _pWeightBlob->clear_data();
    _pWeightBlob->clear_diff();

    const float hScale = height/(float)newHeight;
    const float wScale = width/(float)newWidth;

    weightBlob->mutable_shape()->set_dim(2,newHeight);
    weightBlob->mutable_shape()->set_dim(3,newWidth);
    _pWeightBlob->mutable_shape()->set_dim(2,newHeight);
    _pWeightBlob->mutable_shape()->set_dim(3,newWidth);

    for (int n=0; n<num; ++n) {
        for (int c=0; c<channels; ++c) {
            for (int h=0; h<newHeight; ++h) {
                float originalH = (h + 0.5)*hScale - 0.5;
                int hi = originalH;
                float hf = originalH - hi;
                for (int w=0; w<newWidth; ++w) {
                    float originalW = (w + 0.5)*wScale - 0.5;
                    int wi = originalW;
                    float wf = originalW - wi;

                    float mm = originalData[     wi +     hi*width + c*width*height + n*width*height*channels];
                    float mp = originalData[     wi + (hi+1)*width + c*width*height + n*width*height*channels];
                    float pm = originalData[ (wi+1) +     hi*width + c*width*height + n*width*height*channels];
                    float pp = originalData[ (wi+1) + (hi+1)*width + c*width*height + n*width*height*channels];

                    float weight = (1-wf)*(1-hf)*mm + (1-wf)*hf*mp + wf*(1-hf)*pm + wf*hf*pp;

                    weightBlob->add_data(weight);
                    _pWeightBlob->add_data(weight);
                    weightBlob->add_diff(0);
                    _pWeightBlob->add_diff(0);
                }
            }
        }

    }

    std::cout << weightBlob->data_size() << std::endl;
    std::cout << _pWeightBlob->data_size() << std::endl << std::endl;

    printNetParameter(param);

}

void weightsPadConv(caffe::NetParameter & param, const std::string layerName,
                    const int padWidth, const int padHeight) {

    int layerIndex = -1;
    int _pLayerIndex = -1;
    if (!getLayerAndPairIndex(layerIndex,_pLayerIndex,param,layerName)) { return; }


    caffe::BlobProto * weightBlob = param.mutable_layer(layerIndex)->mutable_blobs(0);
    caffe::BlobProto * _pWeightBlob = param.mutable_layer(_pLayerIndex)->mutable_blobs(0);

    const int originalSize = weightBlob->data_size();
    float * originalData = new float[originalSize];
    for (int i=0; i<originalSize; ++i) { originalData[i] = weightBlob->data(i); }

    const int num = weightBlob->shape().dim(0);
    const int channels = weightBlob->shape().dim(1);
    const int height = weightBlob->shape().dim(2);
    const int width = weightBlob->shape().dim(3);

    const int newWidth = width + 2*padWidth;
    const int newHeight = height + 2*padHeight;

    param.mutable_layer(layerIndex)->mutable_convolution_param()->set_kernel_size(newWidth);
    param.mutable_layer(_pLayerIndex)->mutable_convolution_param()->set_kernel_size(newWidth);

    weightBlob->clear_data();
    weightBlob->clear_diff();
    _pWeightBlob->clear_data();
    _pWeightBlob->clear_diff();

    weightBlob->mutable_shape()->set_dim(2,newHeight);
    weightBlob->mutable_shape()->set_dim(3,newWidth);
    _pWeightBlob->mutable_shape()->set_dim(2,newHeight);
    _pWeightBlob->mutable_shape()->set_dim(3,newWidth);

    for (int n=0; n<num; ++n) {
        for (int c=0; c<channels; ++c) {
            for (int h=0; h<newHeight; ++h) {
                for (int w=0; w<newWidth; ++w) {
                    float weight;
                    if (h >= padHeight && h < newHeight-padHeight && w >= padWidth && w < newWidth-padWidth) {
                        weight = originalData[(w-padWidth) + (h-padHeight)*width + c*width*height + n*width*height*channels];
                    } else {
                        weight = 0;
                    }

                    weightBlob->add_data(weight);
                    _pWeightBlob->add_data(weight);
                    weightBlob->add_diff(0);
                    _pWeightBlob->add_diff(0);
                }
            }
        }

    }

    std::cout << weightBlob->data_size() << std::endl;
    std::cout << _pWeightBlob->data_size() << std::endl << std::endl;

    printNetParameter(param);

}

void weightsShaveInnerProduct(caffe::NetParameter & param, const std::string layerName,
                              const int shaveWidth, const int shaveHeight,
                              const int originalInputChannels, const int originalInputWidth, const int originalInputHeight) {

    int layerIndex = -1;
    int _pLayerIndex = -1;
    if (!getLayerAndPairIndex(layerIndex,_pLayerIndex,param,layerName)) { return; }

    caffe::BlobProto * weightBlob = param.mutable_layer(layerIndex)->mutable_blobs(0);
    caffe::BlobProto * _pWeightBlob = param.mutable_layer(_pLayerIndex)->mutable_blobs(0);

    const int originalSize = weightBlob->data_size();
    float * originalData = new float[originalSize];
    for (int i=0; i<originalSize; ++i) { originalData[i] = weightBlob->data(i); }

    const int num = weightBlob->shape().dim(0);
    const int newWidth = originalInputWidth - 2*shaveWidth;
    const int newHeight = originalInputHeight - 2*shaveHeight;

    weightBlob->mutable_shape()->set_dim(1,originalInputChannels*newWidth*newHeight);
    _pWeightBlob->mutable_shape()->set_dim(1,originalInputChannels*newWidth*newHeight);

    weightBlob->clear_data();
    weightBlob->clear_diff();
    _pWeightBlob->clear_data();
    _pWeightBlob->clear_diff();

    for (int n=0; n<num; ++n) {
        for (int c=0; c<originalInputChannels; ++c) {
            for (int h=shaveHeight; h<originalInputHeight-shaveHeight; ++h) {
                for (int w=shaveWidth; w<originalInputWidth-shaveWidth; ++w) {
                    float weight = originalData[ w + h*originalInputWidth + c*originalInputWidth*originalInputHeight + n*originalInputWidth*originalInputHeight*originalInputChannels];

                    weightBlob->add_data(weight);
                    _pWeightBlob->add_data(weight);
                    weightBlob->add_diff(0);
                    _pWeightBlob->add_diff(0);
                }
            }
        }
    }

    std::cout << weightBlob->data_size() << std::endl;
    std::cout << _pWeightBlob->data_size() << std::endl << std::endl;

    printNetParameter(param);

}


#endif // WEIGHTTRANSFER_H
