#pragma once
#include <pangolin/pangolin.h>
#include <GL/freeglut.h>
#include <iostream>
#include <Eigen/Dense>
#include <caffe/caffe.hpp>
#include <caffe/common.hpp>
#include <caffe/util/rng.hpp>
#include <caffe/util/upgrade_proto.hpp>
#include <vector_types.h>
#include <endian.h>

#include <thrust/host_vector.h>
#include <pangolin/video/drivers/pango_video.h>
#include <pangolin/gl/gltexturecache.h>

#include "weightTransfer.h"
#include "denseNetGeneration.h"
#include <pangolin/video/drivers/openni2.h>
#include "utils.h"
#include <OpenNI.h>
#include <PS1080.h>


// Simple image wrapper
template<typename T>
struct Image {
    inline Image()
        : pitch(0), ptr(0), w(0), h(0)
    {
    }

    inline Image(size_t w, size_t h, size_t pitch, T * ptr)
        : pitch(pitch), ptr(ptr), w(w), h(h)
    {
    }

    void Dealloc()
    {
        if(ptr) {
            delete[] ptr;
            ptr = NULL;
        }
    }

    void Alloc(size_t w, size_t h, size_t pitch)
    {
        Dealloc();
        this->w = w;
        this->h = h;
        this->pitch = pitch;
        this->ptr = (T*)(new unsigned char[h*pitch]);
    }

    void Alloc(size_t w, size_t h)
    {
        Dealloc();
        this->w = w;
        this->h = h;
        this->pitch = sizeof(T)*w;
        this->ptr = new T [w*h];
    }

    size_t pitch;
    T* ptr;
    size_t w;
    size_t h;
};


template <typename DataType = float>
struct DFusionDataset {
    std::vector<float> liveDepth; //vector of vertices in canonical model
    std::vector<float3> canonicalVerts; //vector of vertices in canonical model
    std::vector<float3> canonicalNorms;
    std::vector<uchar3> rgb8uc3;          //per frame RGB image
    std::vector<float4> denseVertRender; //dense rendering of canonical model as mesh, coloured with canonical verts.
    std::vector<float4> projectedVertCorrespondence;
    std::vector< std::vector< Image<DataType> > > pyramid_32f;
    int widthImage, heightImage, numFrames;
};


inline std::string intToString(int number, int leadnums)
{
    std::stringstream ss;//create a stringstream
    ss << std::setfill('0') << std::setw(leadnums) << number;//add number to the stream
    return ss.str();//return a string with the contents of the stream
}



inline float4 glColorHSV( GLfloat hue, GLfloat s=1.0f, GLfloat v=1.0f )
{
    const GLfloat h = 360.0f*hue / 60.0f;
    const int i = (int)floor(h);
    const GLfloat f = (i%2 == 0) ? 1-(h-i) : h-i;
    const GLfloat m = v * (1-s);
    const GLfloat n = v * (1-s*f);
    switch(i)
    {
    case 0: return make_float4(v,n,m,1);
    case 1: return make_float4(n,v,m,1);
    case 2: return make_float4(m,v,n,1);
    case 3: return make_float4(m,n,v,1);
    case 4: return make_float4(n,m,v,1);
    case 5: return make_float4(v,m,n,1);
    default:
        return make_float4(0,0,0,1);
    }
}


inline std::ostream& operator<< (std::ostream& os, const uint3& o){
    os << o.x << " " << o.y << " " << o.z;
    return os;
}

inline std::ostream& operator<< (std::ostream& os, const int4& o){
    os << o.x << " " << o.y << " " << o.z << " " << o.w;
    return os;
}

inline std::istream& operator>> (std::istream& is, uint3& o){
    is >> o.x;
    is >> o.y;
    is >> o.z;
    return is;
}

inline std::istream& operator>> (std::istream& is, int3& o){
    is >> o.x;
    is >> o.y;
    is >> o.z;
    return is;
}


inline std::istream& operator>> (std::istream& is, uint2& o){
    is >> o.x;
    is >> o.y;
    return is;
}


inline std::istream& operator>> (std::istream& is, float2& o){
    is >> o.x;
    is >> o.y;
    return is;
}

inline std::ostream& operator<<(std::ostream& os, const int2 & p){
    os << p.x << ", " << p.y;
    return os;
}

inline std::ostream& operator<<(std::ostream& os, const uint2 & p){
    os << p.x << ", " << p.y;
    return os;
}


inline std::ostream& operator<<(std::ostream& os, const int3 & p){
    os << p.x << ", " << p.y <<", "  << p.z;
    return os;
}



inline std::ostream& operator<<(std::ostream& os, const float2 & p){
    os << p.x << ", " << p.y;
    return os;
}

inline std::ostream& operator<<(std::ostream& os, const float3 & p){
    os << p.x << ", " << p.y << ", " << p.z ;
    return os;
}


inline std::ostream& operator<<(std::ostream& os, const float4 & p){
    os << p.x << ", " << p.y << ", " << p.z << ", " << p.w;
    return os;
}

namespace pangolin{
template<>
struct GlFormatTraits<uchar3>
{
    static const GLint glinternalformat = GL_RGB;
    static const GLenum glformat = GL_RGB;
    static const GLenum gltype = GL_UNSIGNED_BYTE;
};
}

template<typename T>
void RenderToViewport(const T * image_ptr, const int width, const int height, bool flipx=false, bool flipy=false, bool useLinearInterpolation=true)
{
    if (image_ptr) {
        // Retrieve texture that is at least as large as image and of appropriate type.
        pangolin::GlTexture& tex = pangolin::TextureCache::I().GlTex<T>(width, height);
        if(useLinearInterpolation){
            tex.SetLinear();
        }else{
            tex.SetNearestNeighbour();
        }

        tex.Upload(image_ptr, 0, 0, width, height, pangolin::GlFormatTraits<T>::glformat, pangolin::GlFormatTraits<T>::gltype);
        tex.RenderToViewport(pangolin::Viewport(0, 0, width, height), flipx, flipy);
    }
}


bool copyPatchFromPyramid(float * patch,std::vector< Image<float> >  & pyramid,
                          const int widthPatch,const int heightPatch,
                          const int pyramidLevel,
                          const int px,        const int py ){


    const int widthImg = pyramid[pyramidLevel].w;
    const int heightImg = pyramid[pyramidLevel].h;

    int startX = (px>>pyramidLevel) -widthPatch/2;
    int startY = (py>>pyramidLevel)-heightPatch/2;
    int endX = (px>>pyramidLevel) +widthPatch/2;
    int endY = (py>>pyramidLevel) +heightPatch/2;

    //if(startX<0 || endX>= widthImg || startY<0 || endY>=heightImg ) return false;

    for(int x = 0 ; x < widthPatch; x++){
        for(int y = 0 ; y < heightPatch; y++){

            const int img_x  = (x + startX);
            const int img_y =  (y + startY);
            if(img_x<0 || img_x>=widthImg || img_y<0 || img_y>=heightImg ){
                patch[x + y*widthPatch] = rand()/(float)RAND_MAX;
            }else{
                patch[x + y*widthPatch] = pyramid[pyramidLevel].ptr[img_x + img_y*widthImg];
            }
        }
    }

    return true;

}

bool copyPatchFromPyramid(float * patch,std::vector< Image<float3> > & pyramid,
                          const int widthPatch,const int heightPatch,
                          const int pyramidLevel,
                          const int px,        const int py ){

    const int widthImg = pyramid[pyramidLevel].w;
    const int heightImg = pyramid[pyramidLevel].h;

    int startX = (px>>pyramidLevel) - widthPatch/2;
    int startY = (py>>pyramidLevel)- heightPatch/2;
    int endX = (px>>pyramidLevel) + widthPatch/2;
    int endY = (py>>pyramidLevel) + heightPatch/2;

    //    if(startX<0 || endX>= widthImg || startY<0 || endY>=heightImg ) return false;

    for(int x = 0 ; x < widthPatch; x++){
        for(int y = 0 ; y < heightPatch; y++){

            const int img_x  = (x + startX);
            const int img_y =  (y + startY);
            if(img_x<0 || img_x>=widthImg || img_y<0 || img_y>=heightImg ){
                patch[x + y*widthPatch                           ] = rand()/(float)RAND_MAX;
                patch[x + y*widthPatch + 1*widthPatch*heightPatch] = rand()/(float)RAND_MAX;
                patch[x + y*widthPatch + 2*widthPatch*heightPatch] = rand()/(float)RAND_MAX;
            }else{
                patch[x + y*widthPatch] =                            pyramid[pyramidLevel].ptr[img_x + img_y*widthImg].x;
                patch[x + y*widthPatch + 1*widthPatch*heightPatch] = pyramid[pyramidLevel].ptr[img_x + img_y*widthImg].y;
                patch[x + y*widthPatch + 2*widthPatch*heightPatch] = pyramid[pyramidLevel].ptr[img_x + img_y*widthImg].z;
            }
        }
    }

    return true;

}

bool copyPatchFromPyramid(float * patch,std::vector< Image<float4> > & pyramid,
                          const int widthPatch,const int heightPatch,
                          const int pyramidLevel,
                          const int px,        const int py ){

    const int widthImg = pyramid[pyramidLevel].w;
    const int heightImg = pyramid[pyramidLevel].h;

    int startX = (px>>pyramidLevel) - widthPatch/2;
    int startY = (py>>pyramidLevel)- heightPatch/2;
    int endX = (px>>pyramidLevel) + widthPatch/2;
    int endY = (py>>pyramidLevel) + heightPatch/2;

    //    if(startX<0 || endX>= widthImg || startY<0 || endY>=heightImg ) return false;

    for(int x = 0 ; x < widthPatch; x++){
        for(int y = 0 ; y < heightPatch; y++){

            const int img_x  = (x + startX);
            const int img_y =  (y + startY);
            if(img_x<0 || img_x>=widthImg || img_y<0 || img_y>=heightImg ){
                patch[x + y*widthPatch                           ] = rand()/(float)RAND_MAX;
                patch[x + y*widthPatch + 1*widthPatch*heightPatch] = rand()/(float)RAND_MAX;
                patch[x + y*widthPatch + 2*widthPatch*heightPatch] = rand()/(float)RAND_MAX;
                patch[x + y*widthPatch + 3*widthPatch*heightPatch] = rand()/(float)RAND_MAX;
            }else{
                patch[x + y*widthPatch] =                            pyramid[pyramidLevel].ptr[img_x + img_y*widthImg].x;
                patch[x + y*widthPatch + 1*widthPatch*heightPatch] = pyramid[pyramidLevel].ptr[img_x + img_y*widthImg].y;
                patch[x + y*widthPatch + 2*widthPatch*heightPatch] = pyramid[pyramidLevel].ptr[img_x + img_y*widthImg].z;
                patch[x + y*widthPatch + 3*widthPatch*heightPatch] = pyramid[pyramidLevel].ptr[img_x + img_y*widthImg].w;
            }
        }
    }

    return true;

}

template <typename DataType>
DataType linearPixelSample(const DataType * image, const int imageWidth, const float px, const float py) {

    int ix = px;
    int iy = py;
    float tx = px - ix;
    float ty = py - iy;
    return ((1-tx)*(1-ty)*image[ix + iy*imageWidth] +
            (1-tx)*ty*image[ix + (iy+1)*imageWidth] +
            tx*(1-ty)*image[ix+1 + iy*imageWidth] +
            tx*ty*image[ix+1 + (iy+1)*imageWidth]);

}

bool copyPatchFromPyramidLogPolar(float * patch,std::vector< Image<float> >  & pyramid,
                                  const float minR, const float maxR,
                                  const int nRhoSamples, const int nThetaSamples,
                                  const int pyramidLevel,
                                  const int px,        const int py ){


    const int widthImg = pyramid[pyramidLevel].w;
    const int heightImg = pyramid[pyramidLevel].h;

    int startX = (px>>pyramidLevel) - maxR;
    int startY = (py>>pyramidLevel)- maxR;
    int endX = (px>>pyramidLevel) + maxR;
    int endY = (py>>pyramidLevel) + maxR;

    if(startX<0 || endX>= widthImg || startY<0 || endY>=heightImg ) return false;

    const float maxLogR = log(maxR);
    const float minLogR = log(minR);
    const float logStepR = (maxLogR-minLogR)/(float)(nRhoSamples-1);

    for (int r=0; r<nRhoSamples; ++r) {
        float radius = exp(minLogR + r*logStepR);
        for (int t=0; t<nThetaSamples; ++t) {
            float theta = t*M_PI*2.0/(float)nThetaSamples;
            float x = px + radius*cos(theta);
            float y = py + radius*sin(theta);
            patch[r + t*nRhoSamples] = linearPixelSample(pyramid[pyramidLevel].ptr,widthImg,x,y);
        }
    }

    return true;

}

bool copyPatchFromPyramidLogPolar(float * patch,std::vector< Image<float3> >  & pyramid,
                                  const float minR, const float maxR,
                                  const int nRhoSamples, const int nThetaSamples,
                                  const int pyramidLevel,
                                  const int px,        const int py ){


    const int widthImg = pyramid[pyramidLevel].w;
    const int heightImg = pyramid[pyramidLevel].h;

    int startX = (px>>pyramidLevel) - maxR;
    int startY = (py>>pyramidLevel)- maxR;
    int endX = (px>>pyramidLevel) + maxR;
    int endY = (py>>pyramidLevel) + maxR;

    if(startX<0 || endX>= widthImg || startY<0 || endY>=heightImg ) return false;

    const float maxLogR = log(maxR);
    const float minLogR = log(minR);
    const float logStepR = (maxLogR-minLogR)/(float)(nRhoSamples-1);

    for (int r=0; r<nRhoSamples; ++r) {
        float radius = exp(minLogR + r*logStepR);
        for (int t=0; t<nThetaSamples; ++t) {
            float theta = t*M_PI*2.0/(float)nThetaSamples;
            float x = px + radius*cos(theta);
            float y = py + radius*sin(theta);
            float3 v = linearPixelSample(pyramid[pyramidLevel].ptr,widthImg,x,y);
            patch[r + t*nRhoSamples] =                               v.x;
            patch[r + t*nRhoSamples + 1*nRhoSamples*nThetaSamples] = v.y;
            patch[r + t*nRhoSamples + 2*nRhoSamples*nThetaSamples] = v.z;
        }
    }

    return true;

}

bool copyPatchFromPyramidLogPolar(float * patch,std::vector< Image<float4> >  & pyramid,
                                  const float minR, const float maxR,
                                  const int nRhoSamples, const int nThetaSamples,
                                  const int pyramidLevel,
                                  const int px,        const int py ){


    const int widthImg = pyramid[pyramidLevel].w;
    const int heightImg = pyramid[pyramidLevel].h;

    int startX = (px>>pyramidLevel) - maxR;
    int startY = (py>>pyramidLevel)- maxR;
    int endX = (px>>pyramidLevel) + maxR;
    int endY = (py>>pyramidLevel) + maxR;

    if(startX<0 || endX>= widthImg || startY<0 || endY>=heightImg ) return false;

    const float maxLogR = log(maxR);
    const float minLogR = log(minR);
    const float logStepR = (maxLogR-minLogR)/(float)(nRhoSamples-1);

    for (int r=0; r<nRhoSamples; ++r) {
        float radius = exp(minLogR + r*logStepR);
        for (int t=0; t<nThetaSamples; ++t) {
            float theta = t*M_PI*2.0/(float)nThetaSamples;
            float x = px + radius*cos(theta);
            float y = py + radius*sin(theta);
            float4 v = linearPixelSample(pyramid[pyramidLevel].ptr,widthImg,x,y);
            patch[r + t*nRhoSamples] =                               v.x;
            patch[r + t*nRhoSamples + 1*nRhoSamples*nThetaSamples] = v.y;
            patch[r + t*nRhoSamples + 2*nRhoSamples*nThetaSamples] = v.z;
            patch[r + t*nRhoSamples + 3*nRhoSamples*nThetaSamples] = v.w;
        }
    }

    return true;

}

void rgb2grey(const Image<uchar3> & rgb, Image<float3> & grey){
    for(int sx = 0 ; sx < rgb.w;sx++){
        for(int sy = 0 ; sy < rgb.h;sy++){
            float vg =0;
            uchar3 val = rgb.ptr[sx + (sy)*rgb.w];
            grey.ptr[sx + (sy)*grey.w] = make_float3(val.x,val.y,val.z)/(255.0);
        }
    }
}

void rgb2grey(const Image<uchar3> & rgb, Image<float> & grey){

    for(int sx = 0 ; sx < rgb.w;sx++){
        for(int sy = 0 ; sy < rgb.h;sy++){

            float vg =0;
            uchar3 val = rgb.ptr[sx + (sy)*rgb.w];
            vg+= ((float)val.x+(float)val.y + (float)val.z);
            grey.ptr[sx + (sy)*grey.w] = vg/(255.0*3.0);

        }
    }

}

void rgb2grey(const Image<uchar3> & rgb, Image<float4> & grey){
    std::cerr << "shouldn't be here" << std::endl;
}

void rgbAndDepth2RGBD(const Image<uchar3> & rgb,const Image<float> & depth, Image<float> grey) {
    std::cerr << "shouldn't be here" << std::endl;
}

void rgbAndDepth2RGBD(const Image<uchar3> & rgb,const Image<float> & depth, Image<float3> grey) {
    std::cerr << "shouldn't be here" << std::endl;
}

void rgbAndDepth2RGBD(const Image<uchar3> & rgb,const Image<float> & depth, Image<float4> grey) {
    for(int sx = 0 ; sx < rgb.w; sx++){
        for(int sy = 0 ; sy < rgb.h; sy++){
            float vg =0;
            uchar3 rgbVal = rgb.ptr[sx + (sy)*rgb.w];
            float depthVal = depth.ptr[sx + (sy)*depth.w];
            grey.ptr[sx + (sy)*grey.w] = make_float4(rgbVal.x/255.f,rgbVal.y/255.f,rgbVal.z/255.f,depthVal);
        }
    }
}

void imageFloatToChar(const Image<float3> & floatImage, Image<uchar3> & charImage ) {
    for (int y=0; y<floatImage.h; ++y) {
        for(int x=0; x<floatImage.w; ++x) {
            float3 val = floatImage.ptr[x + y*floatImage.w];
            charImage.ptr[x + y*charImage.w] = make_uchar3(val.x*255,val.y*255,val.z*255);
        }
    }
}

void imageFloatToChar(const Image<float> & floatImage, Image<uchar3> & charImage ) {
    for (int y=0; y<floatImage.h; ++y) {
        for(int x=0; x<floatImage.w; ++x) {
            float val = floatImage.ptr[x + y*floatImage.w];
            charImage.ptr[x + y*charImage.w] = make_uchar3(val*255,val*255,val*255);
        }
    }
}

template <typename T>
void ComputeScalarPyramid(std::vector< Image<T> >  & pyramid)
{

    const int p_levels = pyramid.size();
    for (unsigned int l = 1; l < p_levels; ++l) {
        Image<T>& dst_img = pyramid[l];
        Image<T>& src_img = pyramid[l - 1];

        for (unsigned int y = 0; y < dst_img.h; ++y) {
            T* dst = &(dst_img.ptr[y*dst_img.w]);
            T* r1 =  &(src_img.ptr[ (y<<1)*src_img.w]);
            T* r2 =  &(src_img.ptr[ ((y<<1) +1)*src_img.w]);
            for (unsigned int x = 0; x < dst_img.w; ++x) {
                *(dst++) = static_cast<T>((r1[0] + r1[1] + r2[0] + r2[1]) / 4.0f);
                r1 += 2;
                r2 += 2;
            }
        }
    }
}



template <typename DataType>
inline void loadDenseDFusionImages(DFusionDataset<DataType> & dataset,
                            const std::string directory_sn,
                            bool (* frameAcceptFunction)(int frame) = [](int frame) { return true; }
){
    //    std::string fname ="/tmp/pang.png";

    //    pangolin::TypedImage im = pangolin::LoadImage(fname);

    //    std::cout << fname << std::endl;
    //    std::cout << im.w << std::endl;
    //    std::cout << im.h << std::endl;
    //    std::cout << im.pitch << std::endl;
    //    std::cout << im.fmt.channels << std::endl;
    //    for(int i = 0  ; i <im.fmt.channels ;i++ ) std::cout <<"channel " << i << " = "<<  im.fmt.channel_bits[0] << std::endl;

    //    thrust::host_vector<float4> h_verts = d_verts_unwarped;
    //    thrust::host_vector<float4> h_normals = d_normals_unwarped;

    dataset.numFrames = 0;

    std::ifstream stream(directory_sn + "/canonicalModelVerts.txt");

    int num_verts;
    std::string tmp;
    stream >> tmp;
    stream >> num_verts;

    std::cout << num_verts << " num_verts" << std::endl;

    float3 vert;
    float3 norm;
    for (int v = 0; v < num_verts; ++v) {
        stream >> vert.x;
        stream >> vert.y;
        stream >> vert.z;
        dataset.canonicalVerts.push_back(vert);

        stream >> norm.x;
        stream >> norm.y;
        stream >> norm.z;
        dataset.canonicalNorms.push_back(norm);

        //std::cout << v << " vert " << vert.x << " " << vert.y << " " << vert.z << std::endl;
        //std::cout << v << " norm " << norm.x << " " << norm.y << " " << norm.z << std::endl;
    }

    pangolin::VideoInput fusion_rgb;
    pangolin::VideoInput fusion_depth;
    pangolin::VideoInput fusion_verts_dense_rendered;
    pangolin::VideoInput fusion_verts_proj;


    fusion_rgb.Open( directory_sn + "/rgb.pango");
    fusion_depth.Open( directory_sn + "/depth.pango");
    fusion_verts_dense_rendered.Open( directory_sn + "/vertex_dense.pango");
    fusion_verts_proj.Open( directory_sn + "/vertex_proj.pango");

    dataset.widthImage = fusion_rgb.Width();
    dataset.heightImage = fusion_rgb.Height();
    num_verts = fusion_verts_proj.Width();

    std::cout << "width " << dataset.widthImage << " height " << dataset.heightImage << std::endl;
    std::cout << "num_verts " << num_verts << std::endl;


    std::vector<uchar3> tmp_rgb(dataset.widthImage*dataset.heightImage);
    std::vector<float> tmp_depth(dataset.widthImage*dataset.heightImage);
    std::vector<float4> tmp_verts_f4(dataset.widthImage*dataset.heightImage);
    std::vector<float4> tmp_verts_proj_f4(num_verts);

    // read RGB
    int frame = 0;
    dataset.numFrames=0;
    std::vector<bool> framesAccepted;
    while(fusion_rgb.GrabNewest( (unsigned char *) tmp_rgb.data())){
        framesAccepted.push_back(frameAcceptFunction(frame));
        if (framesAccepted.back()) {
            dataset.rgb8uc3.insert(dataset.rgb8uc3.end(),tmp_rgb.begin(),tmp_rgb.end());
            dataset.numFrames++;
        }
        frame++;
    }
    std::cout << "Read in " << dataset.numFrames << " frames of rgb data" << std::endl;

    // read Depth
    frame=0;
    dataset.numFrames = 0;
    while(fusion_depth.GrabNewest( (unsigned char *) tmp_depth.data())){
        if (framesAccepted[frame]) {
            dataset.liveDepth.insert(dataset.liveDepth.end(),tmp_depth.begin(),tmp_depth.end());
            dataset.numFrames++;
        }
        frame++;
    }
    std::cout << "Read in " << dataset.numFrames << " frames of live depth" << std::endl;

    // preprocess training images
    dataset.pyramid_32f.resize(dataset.numFrames);

    for(int frame = 0 ; frame < dataset.numFrames ; frame++){
        Image<DataType> grey_tmp;
        grey_tmp.Alloc(dataset.widthImage,dataset.heightImage,dataset.widthImage*sizeof(DataType));

        Image<uchar3> rgb_tmp;
        rgb_tmp.ptr = &(dataset.rgb8uc3.data()[frame*dataset.widthImage*dataset.heightImage]);
        rgb_tmp.w = dataset.widthImage;
        rgb_tmp.h = dataset.heightImage;
        rgb_tmp.pitch = dataset.widthImage*sizeof(uchar3);

        Image<float> depth_tmp;
        depth_tmp.ptr = &(dataset.liveDepth.data()[frame*dataset.widthImage*dataset.heightImage]);
        depth_tmp.w = dataset.widthImage;
        depth_tmp.h = dataset.heightImage;
        depth_tmp.pitch = dataset.widthImage*sizeof(float);

        //convert to greyscale from 8
        if (sizeof(DataType)/sizeof(float) == 4) {
            rgbAndDepth2RGBD(rgb_tmp,depth_tmp,grey_tmp);
        } else {
            rgb2grey(rgb_tmp,grey_tmp);
        }

        //generate pyramid
        std::vector< Image<DataType> > & pyramid = dataset.pyramid_32f[frame];
        pyramid.resize(7);
        //pyramid[0] = depth_tmp;
        pyramid[0] = grey_tmp;

        for(int p = 1 ; p < 7;p++){
            const int w = dataset.widthImage>>p;
            const int h = dataset.heightImage>>p;
            pyramid[p].Alloc(w,h, w*sizeof(DataType));
        }
        ComputeScalarPyramid(pyramid);
    }


    frame=0;
    dataset.numFrames = 0;
    while(fusion_verts_dense_rendered.GrabNewest( (unsigned char *) tmp_verts_f4.data())){
        if (framesAccepted[frame]) {
            dataset.denseVertRender.insert(dataset.denseVertRender.end(),tmp_verts_f4.begin(),tmp_verts_f4.end());
            dataset.numFrames++;
        }
        frame++;
    }
    std::cout << "Read in " << dataset.numFrames << " frames of vert dense" << std::endl;



    frame=0;
    dataset.numFrames = 0;
    while(fusion_verts_proj.GrabNewest( (unsigned char *) tmp_verts_proj_f4.data())){
        if (framesAccepted[frame]) {
            dataset.projectedVertCorrespondence.insert(dataset.projectedVertCorrespondence.end(),tmp_verts_proj_f4.begin(),tmp_verts_proj_f4.end());
            dataset.numFrames++;
        }
        frame++;
    }
    std::cout << "Read in " << dataset.numFrames << " frames of projected verts" << std::endl;



}

void printBlobInfo(caffe::Net<float>  * testNet,  std::string blobname ){

    boost::shared_ptr<caffe::Blob<float> > featABlob = testNet->blob_by_name(blobname);

    const int channels= featABlob->channels();
    const int batchsize= featABlob->num();
    const int num_axes= featABlob->num_axes();
    const int width= featABlob->width();
    const int height= featABlob->height();

    //e,g,
    //channels 20.
    //batchsize 200.
    //count 2304000.
    //num_axes 4. //
    //width 24. (this is computed by taking this blobs input layer and subtracting 1/2 the kernel size)
    //height 24. //see above.
    const int count= featABlob->count();  //=width*height*batchsize*channels,. i.e. number of floats in this blob.


    std::cout << " ===================================== " << std::endl;

    std::cout << "INFO::" << blobname << std::endl;
    std::cout << "channels " << channels << "." << std::endl;
    std::cout << "batchsize " << batchsize << "." << std::endl;
    std::cout << "count " << count << "." << std::endl;
    std::cout << "num_axes " << num_axes << "." << std::endl;
    std::cout << "width " << width << "." << std::endl;
    std::cout << "height " << height << "." << std::endl;
    std::cout << " ------------------------------------- \n\n" << std::endl;



}



struct ClickHandler3D : public pangolin::Handler3D
{
    ClickHandler3D(pangolin::OpenGlRenderState& cam_state,
                   std::vector<float3> & mappedPoints,
                   float trans_scale=0.01f)
        :  Handler3D(cam_state,pangolin::AxisNone,trans_scale,0.1),wasPressed(false),enable3DMotion(true), mappedPoints(mappedPoints)
    {
        p3Dclick = make_float3(0,0,0);
        selectedExample=-1;
    }

    void Keyboard(pangolin::View&, unsigned char key, int x, int y, bool pressed)
    {

    }

    int getSelectedExample(float3 & p3Dclick, std::vector<float3> & mappedPoints){
        float minDistSq = 1e20;
        for (int i=0; i<mappedPoints.size(); ++i) {
            float3 diff = mappedPoints[i] - p3Dclick;
            float distSq = dot(diff,diff);
            if (distSq < minDistSq) {
                minDistSq = distSq;
                selectedExample = i;
            }
        }
        return selectedExample;
    }

    void Mouse(pangolin::View& v, pangolin::MouseButton button, int x, int y, bool pressed, int button_state){



        Handler3D::Mouse(v,button,x,y,pressed,button_state);
        //std::cout << "pressed" << "\tPOINT:: " << rot_center[0] << " " << rot_center[1] << " " << rot_center[2] << std::endl;
        if(pressed==true && ( button == pangolin::MouseButtonLeft )){
            p3Dclick = make_float3(Pw[0],Pw[1],Pw[2]);
            glReadPixels (last_pos[0],last_pos[1],1,1,GL_RGB,GL_FLOAT,(GLfloat *)(&selected_pixel_colour));

            //std::cout << (int)(255*last_rs[0]) << ", " <<(int)(255*last_rs[1]) << ", " << (int)(255*last_rs[2]) <<std::endl;

            if(!enable3DMotion){
                selectedExample = getSelectedExample(p3Dclick,mappedPoints);
            }
        }
        wasPressed=pressed;

    }

    void MouseMotion(pangolin::View& v, int x, int y, int button_state){

        if(enable3DMotion){
            Handler3D::MouseMotion(v,x,y,button_state);
        }else{
            last_pos[0] = x;
            last_pos[1] = y;
            GetPosNormal(v,x,y,p,Pw,Pc,n,last_z);
            p3Dclick = make_float3(Pw[0],Pw[1],Pw[2]);

            selectedExample = getSelectedExample(p3Dclick,mappedPoints);


        }

    }


    bool enable3DMotion;
    bool wasPressed;
    float3 p3Dclick;
    //GLfloat last_rs[3];
    float3 selected_pixel_colour;

    std::vector<float3> & mappedPoints;
    int selectedExample;




};

bool copyPatchFromImage(float * patch, const uchar3 * image,
                        const int widthImg,  const int heightImg,
                        const int widthPatch,const int heightPatch,
                        const int subSample,
                        const int px,        const int py ){


    int startX = px-widthPatch*subSample/2;
    int startY = py-heightPatch*subSample/2;
    int endX = px + widthPatch*subSample/2;
    int endY = py + heightPatch*subSample/2;

    if(startX<0 || endX> widthImg || startY<0 || endY>heightImg ) return false;

    const float multNorm = 1.0f/(255.0f*3.0f*subSample*subSample);
    for(int x = 0 ; x < widthPatch; x++){
        for(int y = 0 ; y < heightPatch; y++){

            const int img_x  = (x*subSample + startX);
            const int img_y =  (y*subSample + startY);
            float grey =0;
            for(int sx = 0 ; sx < subSample;sx++){
                for(int sy = 0 ; sy < subSample;sy++){

                    uchar3 val = image[img_x+sx + (img_y+sy)*widthImg];
                    grey+= ((float)val.x+(float)val.y + (float)val.z);

                }
            }
            patch[x + y*widthPatch] = grey*multNorm;

        }
    }

    return true;

}

void startDrawing(float width, float height){

    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho(0, width, height, 0, 0, 1);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity();
    glDepthMask(GL_FALSE);


}

void endDrawing(){
    glDepthMask(GL_TRUE);
}





void showPatchPair(float * imgData, int patchPairNum, int channels, int widthPatch, int heightPatch) {

    glColor3f(1,1,1);
    if (channels == 2) { // grayscale
        RenderToViewport( (float *) (imgData + (patchPairNum*channels)*widthPatch*heightPatch),widthPatch,heightPatch*2,false,true,true);
    } else { // color
        static pangolin::GlTexture patchPairTexture(widthPatch,heightPatch*2);
        static float * patchImage = new float[widthPatch*heightPatch*6];
        for (int c=0; c<3; ++c) {
            for (int y=0; y<heightPatch; ++y) {
                for (int x=0; x<widthPatch; ++x) {
                    patchImage[c + x*3 + y*3*widthPatch] =                            imgData[x + y*widthPatch + c*widthPatch*heightPatch + patchPairNum*6*widthPatch*heightPatch];
                    patchImage[c + x*3 + y*3*widthPatch + 3*widthPatch*heightPatch] = imgData[x + y*widthPatch + (c+3)*widthPatch*heightPatch  + patchPairNum*6*widthPatch*heightPatch];
                }
            }
        }
        patchPairTexture.Upload(patchImage,GL_RGB,GL_FLOAT);
        patchPairTexture.RenderToViewportFlipY();
    }

}

void showFilterVisualization(int channels, int filterWidth, int filterHeight, int filterNum, float * filterViz, const float * filterWeights, const int totalNumFilters) {

    if (channels == 2) { // grayscale

        //            if (filterNum.GuiChanged()) {
        memcpy(filterViz,filterWeights + filterNum*filterWidth*filterHeight,filterWidth*filterHeight*sizeof(float));
        float minVal = 1e10;
        float maxVal = -1e10;
        for (int i=0; i<filterWidth*filterHeight; ++i) {
            minVal = std::min(minVal,filterViz[i]);
            maxVal = std::max(maxVal,filterViz[i]);
        }
        for (int i=0; i<filterWidth*filterHeight; ++i) { filterViz[i] = (filterViz[i]-minVal)/(maxVal-minVal); }
        //            }
        glColor3f(1,1,1);
        RenderToViewport(filterViz,filterWidth,filterHeight,false,true,false);
    } else if (channels == 6) { //color
        float minVal = 1e10;
        float maxVal = -1e10;
        for (int i=0; i<3*filterWidth*filterHeight*totalNumFilters; ++i) {
            minVal = std::min(minVal,filterWeights[i]);
            maxVal = std::max(maxVal,filterWeights[i]);
        }

        for (int y=0; y<filterHeight; ++y) {
            for (int x=0; x<filterWidth; ++x) {
                for (int c=0; c<3; ++c) {
                    filterViz[c + x*3 + y*3*filterWidth] = (filterWeights[x + y*filterWidth + (c+3*filterNum)*filterWidth*filterHeight]-minVal)/(maxVal-minVal);
                }
            }
        }
        static pangolin::GlTexture filterTexture(filterWidth,filterHeight);
        glColor3f(1,1,1);
        filterTexture.SetNearestNeighbour();
        filterTexture.Upload(filterViz,GL_RGB,GL_FLOAT);
        filterTexture.RenderToViewportFlipY();
    } else {
//        float minValRGB = 1e10;
//        float maxValRGB = -1e10;
//        float minValD = 1e10;
//        float maxValD = -1e10;
//        for (int i=0; i<3*filterWidth*filterHeight*totalNumFilters; ++i) {
//            minValRGB = std::min(minValRGB,filterWeights[i]);
//            maxValRGB = std::max(maxValRGB,filterWeights[i]);
//            minValD = std::min(minValD,filterWeights[i]);
//            maxValD = std::max(maxValD,filterWeights[i]);
//        }

//        for (int y=0; y<filterHeight; ++y) {
//            for (int x=0; x<filterWidth; ++x) {
//                for (int c=0; c<3; ++c) {
//                    filterViz[c + x*3 + y*3*filterWidth] = (filterWeights[x + y*filterWidth + (c+3*filterNum)*filterWidth*filterHeight]-minVal)/(maxVal-minVal);
//                }
//            }
//        }
//        static pangolin::GlTexture filterTexture(filterWidth,filterHeight);
//        glColor3f(1,1,1);
//        filterTexture.SetNearestNeighbour();
//        filterTexture.Upload(filterViz,GL_RGB,GL_FLOAT);
//        filterTexture.RenderToViewportFlipY();
    }

}

void showAllFilterGradientVisualization(int channels, int filterWidth, int filterHeight, const float * filterGradients, const int totalNumFilters) {

    const int border = 1;

    if (channels == 2) { // grayscale

        float maxVal = 0;
        for (int i=0; i<filterWidth*filterHeight*totalNumFilters; ++i) {
            maxVal = std::max(maxVal,fabsf(filterGradients[i]));
        }

        static const int N = ceil(sqrt(totalNumFilters));
        static const int vizWidth = (N+1)*border + N*filterWidth;
        static const int vizHeight = (N+1)*border + N*filterHeight;
        //static float * filterViz = new float[vizWidth*vizHeight];
        //memset(filterViz,0,vizWidth*vizHeight*sizeof(float));
        static std::vector<float> filterViz(vizWidth*vizHeight,0.5);

        for (int i=0; i<totalNumFilters; ++i) {
            int c = i % N;
            int r = i / N;
            for (int y=0; y<filterHeight; ++y) {
                for (int x=0; x<filterWidth; ++x) {
                    filterViz[(c+1)*border + c*filterWidth + x +
                            ((r+1)*border + r*filterHeight + y)*vizWidth] =
                            fabsf(filterGradients[x + y*filterWidth + (0+i)*filterWidth*filterHeight]-maxVal)/(2*maxVal);
                }
            }
        }

        static pangolin::GlTexture filterTexture(vizWidth,vizHeight);
        glColor3f(1,1,1);
        filterTexture.SetNearestNeighbour();
        filterTexture.Upload((float*)filterViz.data(),GL_LUMINANCE,GL_FLOAT);
        filterTexture.RenderToViewportFlipY();

    } else if (channels == 6) { //color

        float maxVal = 0;
        for (int i=0; i<3*filterWidth*filterHeight*totalNumFilters; ++i) {
            maxVal = std::max(maxVal,fabsf(filterGradients[i]));
        }

        static const int N = ceil(sqrt(totalNumFilters));
        static const int vizWidth = (N+1)*border + N*filterWidth;
        static const int vizHeight = (N+1)*border + N*filterHeight;
        static float3 * filterViz = new float3[vizWidth*vizHeight];
        memset(filterViz,0,vizWidth*vizHeight*sizeof(float3));

        for (int i=0; i<totalNumFilters; ++i) {
            int c = i % N;
            int r = i / N;
            for (int y=0; y<filterHeight; ++y) {
                for (int x=0; x<filterWidth; ++x) {
                    filterViz[(c+1)*border + c*filterWidth + x +
                            ((r+1)*border + r*filterHeight + y)*vizWidth] = make_float3(
                                (filterGradients[x + y*filterWidth + (0+3*i)*filterWidth*filterHeight]-maxVal)/(2*maxVal),
                            (filterGradients[x + y*filterWidth + (1+3*i)*filterWidth*filterHeight]-maxVal)/(2*maxVal),
                            (filterGradients[x + y*filterWidth + (2+3*i)*filterWidth*filterHeight]-maxVal)/(2*maxVal)
                            );
                }
            }
        }

        static pangolin::GlTexture filterTexture(vizWidth,vizHeight);
        glColor3f(1,1,1);
        filterTexture.SetNearestNeighbour();
        filterTexture.Upload((float*)filterViz,GL_RGB,GL_FLOAT);
        filterTexture.RenderToViewportFlipY();
    } else { // RGBD

        float maxValRGB = 0;
        float maxValD = 0;
        for (int i=0; i<totalNumFilters; ++i) {
            for (int j=0; j<filterWidth*filterHeight; ++j) {
                for (int c=0; c<3; ++c) {
                    maxValRGB = std::max(maxValRGB,fabsf(filterGradients[i*4*filterWidth*filterHeight + c*filterWidth*filterHeight + j]));
                }
                maxValD = std::max(maxValD,fabsf(filterGradients[i*4*filterWidth*filterHeight + 3*filterWidth*filterHeight + j]));
            }
        }

        static const int N = ceil(sqrt(totalNumFilters));
        static const int vizWidth = (2*N+1)*border + 2*N*filterWidth;
        static const int vizHeight = (N+1)*border + N*filterHeight;
        static float3 * filterViz = new float3[vizWidth*vizHeight];
        memset(filterViz,0,vizWidth*vizHeight*sizeof(float3));

        for (int i=0; i<totalNumFilters; ++i) {
            int c = i % N;
            int r = i / N;
            for (int y=0; y<filterHeight; ++y) {
                for (int x=0; x<filterWidth; ++x) {
                    filterViz[(2*c+1)*border + 2*c*filterWidth + x +
                            ((r+1)*border + r*filterHeight + y)*vizWidth] = make_float3(
                                (filterGradients[x + y*filterWidth + (0+4*i)*filterWidth*filterHeight]-maxValRGB)/(2*maxValRGB),
                            (filterGradients[x + y*filterWidth + (1+4*i)*filterWidth*filterHeight]-maxValRGB)/(2*maxValRGB),
                            (filterGradients[x + y*filterWidth + (2+4*i)*filterWidth*filterHeight]-maxValRGB)/(2*maxValRGB)
                            );
                    filterViz[(2*c+1+1)*border + (2*c+1)*filterWidth + x +
                            ((r+1)*border + r*filterHeight + y)*vizWidth] = make_float3((filterGradients[x + y*filterWidth + (3+4*i)*filterWidth*filterHeight]-maxValD)/(2*maxValD));
                }
            }
        }

        static pangolin::GlTexture filterTexture(vizWidth,vizHeight);
        glColor3f(1,1,1);
        filterTexture.SetNearestNeighbour();
        filterTexture.Upload((float*)filterViz,GL_RGB,GL_FLOAT);
        filterTexture.RenderToViewportFlipY();

    }

}

void showAllFilterVisualization(int channels, int filterWidth, int filterHeight, const float * filterWeights, const int totalNumFilters) {

    const int border = 1;

    if (channels == 2) { // grayscale

        float minVal = 1e10;
        float maxVal = -1e10;
        for (int i=0; i<filterWidth*filterHeight*totalNumFilters; ++i) {
            minVal = std::min(minVal,filterWeights[i]);
            maxVal = std::max(maxVal,filterWeights[i]);
        }

        static const int N = ceil(sqrt(totalNumFilters));
        static const int vizWidth = (N+1)*border + N*filterWidth;
        static const int vizHeight = (N+1)*border + N*filterHeight;
        //static float * filterViz = new float[vizWidth*vizHeight];
        //memset(filterViz,0,vizWidth*vizHeight*sizeof(float));
        static std::vector<float> filterViz(vizWidth*vizHeight,0.5);

        for (int i=0; i<totalNumFilters; ++i) {
            int c = i % N;
            int r = i / N;
            for (int y=0; y<filterHeight; ++y) {
                for (int x=0; x<filterWidth; ++x) {
                    filterViz[(c+1)*border + c*filterWidth + x +
                            ((r+1)*border + r*filterHeight + y)*vizWidth] =
                            (filterWeights[x + y*filterWidth + (0+i)*filterWidth*filterHeight]-minVal)/(maxVal-minVal);
                }
            }
        }

        static pangolin::GlTexture filterTexture(vizWidth,vizHeight);
        glColor3f(1,1,1);
        filterTexture.SetNearestNeighbour();
        filterTexture.Upload((float*)filterViz.data(),GL_LUMINANCE,GL_FLOAT);
        filterTexture.RenderToViewportFlipY();

    } else if (channels == 6) { //color

        float minVal = 1e10;
        float maxVal = -1e10;
        for (int i=0; i<3*filterWidth*filterHeight*totalNumFilters; ++i) {
            minVal = std::min(minVal,filterWeights[i]);
            maxVal = std::max(maxVal,filterWeights[i]);
        }

        static const int N = ceil(sqrt(totalNumFilters));
        static const int vizWidth = (N+1)*border + N*filterWidth;
        static const int vizHeight = (N+1)*border + N*filterHeight;
        static float3 * filterViz = new float3[vizWidth*vizHeight];
        memset(filterViz,0,vizWidth*vizHeight*sizeof(float3));

        for (int i=0; i<totalNumFilters; ++i) {
            int c = i % N;
            int r = i / N;
            for (int y=0; y<filterHeight; ++y) {
                for (int x=0; x<filterWidth; ++x) {
                    filterViz[(c+1)*border + c*filterWidth + x +
                            ((r+1)*border + r*filterHeight + y)*vizWidth] = make_float3(
                                (filterWeights[x + y*filterWidth + (0+3*i)*filterWidth*filterHeight]-minVal)/(maxVal-minVal),
                            (filterWeights[x + y*filterWidth + (1+3*i)*filterWidth*filterHeight]-minVal)/(maxVal-minVal),
                            (filterWeights[x + y*filterWidth + (2+3*i)*filterWidth*filterHeight]-minVal)/(maxVal-minVal)
                            );
                }
            }
        }

        static pangolin::GlTexture filterTexture(vizWidth,vizHeight);
        glColor3f(1,1,1);
        filterTexture.SetNearestNeighbour();
        filterTexture.Upload((float*)filterViz,GL_RGB,GL_FLOAT);
        filterTexture.RenderToViewportFlipY();
    } else {

        float minValRGB = 1e10;
        float maxValRGB = -1e10;
        float minValD = 1e10;
        float maxValD = -1e10;
        for (int i=0; i<totalNumFilters; ++i) {
            for (int j=0; j<filterWidth*filterHeight; ++j) {
                for (int c=0; c<3; ++c) {
                    minValRGB = std::min(minValRGB,filterWeights[i*4*filterWidth*filterHeight + c*filterWidth*filterHeight + j]);
                    maxValRGB = std::max(maxValRGB,filterWeights[i*4*filterWidth*filterHeight + c*filterWidth*filterHeight + j]);
                }
                minValD = std::min(minValD,filterWeights[i*4*filterWidth*filterHeight + 3*filterWidth*filterHeight + j]);
                maxValD = std::max(maxValD,filterWeights[i*4*filterWidth*filterHeight + 3*filterWidth*filterHeight + j]);
            }
        }

        static const int N = ceil(sqrt(totalNumFilters));
        static const int vizWidth = (2*N+1)*border + 2*N*filterWidth;
        static const int vizHeight = (N+1)*border + N*filterHeight;
        static float3 * filterViz = new float3[vizWidth*vizHeight];
        memset(filterViz,0,vizWidth*vizHeight*sizeof(float3));

        for (int i=0; i<totalNumFilters; ++i) {
            int c = i % N;
            int r = i / N;
            for (int y=0; y<filterHeight; ++y) {
                for (int x=0; x<filterWidth; ++x) {
                    filterViz[(2*c+1)*border + 2*c*filterWidth + x +
                            ((r+1)*border + r*filterHeight + y)*vizWidth] = make_float3(
                                (filterWeights[x + y*filterWidth + (0+4*i)*filterWidth*filterHeight]-minValRGB)/(maxValRGB-minValRGB),
                            (filterWeights[x + y*filterWidth + (1+4*i)*filterWidth*filterHeight]-minValRGB)/(maxValRGB-minValRGB),
                            (filterWeights[x + y*filterWidth + (2+4*i)*filterWidth*filterHeight]-minValRGB)/(maxValRGB-minValRGB)
                            );
                    filterViz[(2*c+1+1)*border + (2*c+1)*filterWidth + x +
                            ((r+1)*border + r*filterHeight + y)*vizWidth] =
                            make_float3( (filterWeights[x + y*filterWidth + (3+4*i)*filterWidth*filterHeight]-minValD)/(maxValD-minValD) );
                }
            }
        }

        static pangolin::GlTexture filterTexture(vizWidth,vizHeight);
        glColor3f(1,1,1);
        filterTexture.SetNearestNeighbour();
        filterTexture.Upload((float*)filterViz,GL_RGB,GL_FLOAT);
        filterTexture.RenderToViewportFlipY();

    }

}

inline void showAllInnerProductsVisualization(const float * weights, const int outputNum, const int inputChannels, const int inputWidth, const int inputHeight, int visualizedChannel) {

    const int border = 1;

    float minVal = 1e10;
    float maxVal = -1e10;
    for (int n=0; n<outputNum; ++n) {
        for (int i=0; i<inputWidth*inputHeight; ++i) {
            minVal = std::min(minVal,weights[i + visualizedChannel*inputWidth*inputHeight + n*inputWidth*inputHeight*inputChannels]);
            maxVal = std::max(maxVal,weights[i + visualizedChannel*inputWidth*inputHeight + n*inputWidth*inputHeight*inputChannels]);
        }
    }

    static const int N = ceil(sqrt(outputNum));
    static const int vizWidth = (N+1)*border + N*inputWidth;
    static const int vizHeight = (N+1)*border + N*inputHeight;
    //static float * filterViz = new float[vizWidth*vizHeight];
    //memset(filterViz,0,vizWidth*vizHeight*sizeof(float));
    static std::vector<float> filterViz(vizWidth*vizHeight,0.5);

    for (int i=0; i<outputNum; ++i) {
        int c = i % N;
        int r = i / N;
        for (int y=0; y<inputHeight; ++y) {
            for (int x=0; x<inputWidth; ++x) {
                filterViz[(c+1)*border + c*inputWidth + x +
                        ((r+1)*border + r*inputHeight + y)*vizWidth] =
                        (weights[x + y*inputWidth + visualizedChannel*inputWidth*inputHeight + i*inputWidth*inputHeight*inputChannels]-minVal)/(maxVal-minVal);
            }
        }
    }

    static pangolin::GlTexture filterTexture(vizWidth,vizHeight);
    glColor3f(1,1,1);
    filterTexture.SetNearestNeighbour();
    filterTexture.Upload((float*)filterViz.data(),GL_LUMINANCE,GL_FLOAT);
    filterTexture.RenderToViewportFlipY();

}

inline float * getCaffeinatedImage(const Image<float> & unswizzledImage) {
    return unswizzledImage.ptr;
}

inline float * getCaffeinatedImage(const Image<float3> & unswizzledImage) {
    static std::vector<float> caffeinatedColorImage(unswizzledImage.w*unswizzledImage.h*3);
    for (int y=0; y<unswizzledImage.h; ++y) {
        for (int x=0; x<unswizzledImage.w; ++x) {
            for (int c=0; c<3; ++c) {
                caffeinatedColorImage[x + y*unswizzledImage.w + c*unswizzledImage.w*unswizzledImage.h] = ((float*)unswizzledImage.ptr)[c + x*3 + y*3*unswizzledImage.w];
            }
        }
    }
    return caffeinatedColorImage.data();
}

inline float * getCaffeinatedImage(const Image<float4> & unswizzledImage) {
    static std::vector<float> caffeinatedRGBDImage(unswizzledImage.w*unswizzledImage.h*4);
    for (int y=0; y<unswizzledImage.h; ++y) {
        for (int x=0; x<unswizzledImage.w; ++x) {
            for (int c=0; c<4; ++c) {
                caffeinatedRGBDImage[x + y*unswizzledImage.w + c*unswizzledImage.w*unswizzledImage.h] = ((float*)unswizzledImage.ptr)[c + x*4 + y*4*unswizzledImage.w];
            }
        }
    }
    return caffeinatedRGBDImage.data();
}

int findClosestPoint(const float3 queryPoint, const std::vector<float3> & denseMappedPoints) {

    int closestInd = -1;
    float closestDist = 1e10;
    for (int i=0; i<denseMappedPoints.size(); ++i) {
        float dist = length(denseMappedPoints[i] - queryPoint);
        if (dist < closestDist ) {
            closestDist = dist;
            closestInd = i;
        }
    }

    return closestInd;
}
