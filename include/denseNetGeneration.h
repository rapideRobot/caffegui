#ifndef DENSENETGENERATION_H
#define DENSENETGENERATION_H

#include <caffe/caffe.hpp>

void generateDenseTestNet(caffe::NetParameter & dynamicallyGeneratedTestNetParam, const caffe::NetParameter & trainNetParam, const int channels,
                          const int width, const int height, boost::shared_ptr<caffe::Net<float> > & trainNet, const int patchStride = 1) {
    std::cout << "train net: " << std::endl;

    // set up data layer
    caffe::LayerParameter * dataLayerParam = dynamicallyGeneratedTestNetParam.add_layer();
    dataLayerParam->set_name("data");
    dataLayerParam->set_type("MemoryData");
    std::string * dataTop = dataLayerParam->add_top();
    *dataTop = "data";
    std::string * labelTop = dataLayerParam->add_top();
    *labelTop = "sim";

//        dataLayerParam->set_top(0,"data");
//        dataLayerParam->set_top(1,"sim");
    caffe::MemoryDataParameter * memoryDataParam = dataLayerParam->mutable_memory_data_param();
    memoryDataParam->set_batch_size(1);
    memoryDataParam->set_channels(channels);
    memoryDataParam->set_width(width);
    memoryDataParam->set_height(height);

    std::set<std::string> layerTypesToCopy;
    layerTypesToCopy.insert("Convolution");
    layerTypesToCopy.insert("InnerProduct");
    layerTypesToCopy.insert("ReLU");
    layerTypesToCopy.insert("Sigmoid");
    for (int i=0; i<trainNetParam.layer_size(); ++i ) {

        const caffe::LayerParameter & layerParam = trainNetParam.layer(i);
        std::cout << "   layer " << i << ": " << layerParam.name() << std::endl;
        std::cout << "             " << layerParam.type() << std::endl;

        if (layerTypesToCopy.find(layerParam.type()) != layerTypesToCopy.end()) {

            if (layerParam.name().substr(layerParam.name().length()-2,2) == "_p") { // don't copy siamese part
                continue;
            }

            std::cout << "             copy this layer to test" << std::endl;

            caffe::LayerParameter * copiedLayerParam = dynamicallyGeneratedTestNetParam.add_layer();
            *copiedLayerParam = layerParam;

            if (layerParam.type() == "InnerProduct") {
                static bool firstInnerProduct = true;

                copiedLayerParam->set_type("Convolution"); // turn IP into a convolution
                copiedLayerParam->clear_inner_product_param();

                caffe::ConvolutionParameter * convParam = copiedLayerParam->mutable_convolution_param();
                convParam->set_num_output( layerParam.inner_product_param().num_output());
                convParam->set_stride(1);

                if (firstInnerProduct) {

                    std::cout << "first inner product is " << layerParam.name() << std::endl;

                    const std::string bottom = layerParam.bottom(0);
                    boost::shared_ptr<caffe::Blob<float> > bottomBlob = trainNet->blob_by_name(bottom);
                    const int bottomBlobWidth = bottomBlob->width();
                    const int bottomBlobHeight = bottomBlob->height();

                    std::cout << "top blob is " << bottomBlobWidth << " x " << bottomBlobHeight << std::endl;

                    convParam->set_kernel_size(bottomBlobWidth);
                    convParam->set_kernel_size(bottomBlobHeight);
                    convParam->set_pad_h(bottomBlobHeight/2);
                    convParam->set_pad_w(bottomBlobWidth/2);
                    convParam->set_stride(patchStride);

                    firstInnerProduct = false;
                } else {
                    convParam->set_kernel_size(1);
                }
            } else if (layerParam.type() == "Convolution") {

                caffe::ConvolutionParameter * convParam = copiedLayerParam->mutable_convolution_param();
                convParam->set_pad(convParam->kernel_size()/2);
            }
        }
    }

    std::cout << "dynamically generated test net: " << std::endl;
    for (int i=0; i<dynamicallyGeneratedTestNetParam.layer_size(); ++i ) {

        std::cout << "   layer " << i << ": " << dynamicallyGeneratedTestNetParam.layer(i).name() << std::endl;

    }
    caffe::WriteProtoToTextFile(dynamicallyGeneratedTestNetParam,"/tmp/test.prototxt");

//        std::cout << "test net: " << std::endl;
//        caffe::NetParameter testNetParam;
//        caffe::ReadNetParamsFromTextFileOrDie("networks/dense_test_1conv.prototxt",&testNetParam);
//        for (int i=0; i<testNetParam.layer_size(); ++i ) {

//            std::cout << "   layer " << i << ": " << testNetParam.layer(i).name() << std::endl;

//        }
}


#endif // DENSENETGENERATION_H
